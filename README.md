
# Game
This game is based around the idea of rotating stripes, or rows/columns on a 
toroidal surface to match a certain pattern made out of rectangular tiles. The 
pattern itself is unknown. Only the delta pattern for each row and column is known.

Update 27/02: I found out this thing is called a nonogram. But this game actually
gives it a twist, in that you don't just edit the cells. You have to move rows
and columns to complete it.

# tl;dr
Move rows and columns to match the pattern printed at the side and top of the board.

# Mobile & web
There are plans for a web version that will use one of the ports of Löve2D to js.
However, tests have shown to be extremely slow. I'll have to optimize the game for
HTML5 in the future, likely after the release of the main game.

But the Android version is functional and the time of writing this (10/03) it's
functionality is exactly the same as that of the desktop version. They're also
completely compatible with each other, meaning all levels and all files can be 
exchanged without modifying a line. 

An iPhone version is theoretically possible and would likely work as expected with
only minor tweaks, but due too the App Store policy and difficulties with creating
an iOS build, the iOS version is either cancelled or indefinitely postponed.
Don't expect an iOS version. I cannot guarantee whether or not it will exist.

# Goals
- **week 1 (20/02 - 26/02)**
- [x] core gameplay
- [x] toroid topology
- [x] better rendering
- [x] main menu
- [x] mini menu
- [x] gamemodes
- [x] gamemode: normal
- **week 2 (27/02 - 05/03)**
- [x] better menus (lua scripted)
- [x] level editor
- [x] gamemode: normal levels
- [x] gamemode: color
- **week 3 (06/03 - 12/03)**
- [x] score & timer
- [x] gamemode: color levels
- [x] beta release on itch.io (Linux, Windows, maybe web and Mac)
- [x] Android test
- **week 4 (13/03 - 19/03)**
- [x] options menu
 - [x] colorblind mode
 - [x] vibrations
- [x] better graphics
- [x] better gui (visuals)
- [x] web test
- **week 5 (20/03 - 26/03)**
- [x] bugfixes
- [x] tweaks
- **week 6 (27/03 - 02/04)**
- [ ] desktop release
- [ ] Android release
- **tbd**
- [ ] web release
- **canceled**
- options
 - sound (canceled as of 18/03/17)
 - left-handed (canceled as of 18/03/17)
- iPhone release (canceled as of 07/03/17)

# Known bugs:
none

# Build instructions
- `make love` creates a .love file which is necessary for other builds.
- `make <platform>` creates a build for a specific `<platform>`.
- `make push-<platform>` pushes the build for `<platform>` to itch.io using butler.
Use `make all` or `make love <platform>` to build safely (all targets or for a specific platform).
