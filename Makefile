
VERSION = 1.0c

.PHONY: love linux windows mac android

default: love

all: love linux windows mac android

love:
	rm release/nonoroto.love
	cd src; zip -9 -r ../release/nonoroto.love *.lua
	zip -9 -r release/nonoroto.love lvl fonts/Akt_smooth.ttf LICENSE

linux:
	cp release/nonoroto.love release/linux/

windows:
	cat release/windows/love.exe release/nonoroto.love > release/windows/nonoroto.exe

mac:
	cp release/nonoroto.love release/mac/NoNoRotoGram.app/Contents/Resources/

android:
	cp release/nonoroto.love release/android/love-android-sdl2/assets/game.love
	cd release/android/love-android-sdl2/; ant debug
	cp release/android/love-android-sdl2/bin/love-android-debug.apk release/nonoroto_android/nonoroto_android.apk

web:
	cd ~/git/nonorotogram-game/release/web/love.js/release-compatibility; \
	python2 ../emscripten/tools/file_packager.py game.data --preload game@/ --js-output=game.js; \
	zip -9 -r ~/git/nonorotogram-game/release/nono-rotogram-web.zip *

push-all: push-src push-linux push-windows push-mac push-android

push-src:
	butler push release/nonoroto.love pi-pi3/nono-rotogram:src --userversion $(VERSION)

push-linux:
	butler push --fix-permissions release/linux pi-pi3/nono-rotogram:linux --userversion $(VERSION)

push-windows:
	butler push release/windows pi-pi3/nono-rotogram:win --userversion $(VERSION)

push-mac:
	butler push --fix-permissions release/mac pi-pi3/nono-rotogram:mac --userversion $(VERSION)

push-android:
	butler push release/nonoroto_android pi-pi3/nono-rotogram:android --userversion $(VERSION)
