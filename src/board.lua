
--[[ board.lua Board functionality for this game.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local table = require('table')
local util = require('utils')

local board = {}

function board.new(w, h, p, colors)
    local b = {}
    b.width = w
    b.height = util.default(h, w)

    colors = util.default(colors, 3)
    p = util.default(p, 0.5)
    local n = math.floor(w*h*p+0.5)

    for y = 1, h do
        b[y] = {}
        for x = 1, w do
            b[y][x] = 0
        end
    end

    for i = 1, n do 
        local done = false
        while not done do
            local x = math.random(1, w)
            local y = math.random(1, h)
            local c = math.random(1, colors)
            
            if b[y][x] == 0 then
                b[y][x] = c
                done = true
            end
        end
    end

    return b
end

function board.shuffle(b, n)
    local new = util.deepcopy(b)
    for i = 1, n do
        local rotate = (math.random(0,1) == 1) and 'x' or 'y'
        if rotate == 'y' then
            local col = math.random(1, new.width)
            local n = math.random(1, new.height-1)
            board.rotate_y(new, col, n)
        elseif rotate == 'x' then
            local row = math.random(1, new.height)
            local n = math.random(1, new.height-1)
            board.rotate_x(new, row, n)
        end
    end

    return new
end

function board.rotate_x(b, row, dir)
    local new = {}

    for x = 1, b.width do
        local x1 = (x-dir)%b.width
        if x1 == 0 then x1 = b.width end
        new[x] = b[row][x1]
    end

    b[row] = new
end

function board.rotate_y(b, col, dir)
    local new = {}

    for y = 1, b.height do
        local y1 = (y-dir)%b.height
        if y1 == 0 then y1 = b.height end
        new[y] = b[y1][col]
    end

    for y = 1, b.height do
        b[y][col] = new[y]
    end
end

function board.check_row(b, row)
    local delta = {}
    local i = 0
    local gap = false
    for x = 1, b.width do
        if not gap and delta[i] and b[row][x] == delta[i].c then
            delta[i].n = delta[i].n + 1
            gap = false
        elseif b[row][x] ~= 0 then
            table.insert(delta, {c = b[row][x], n = 1})
            i = #delta
            gap = false
        else
            gap = true
        end
    end

    return delta
end

function board.check_col(b, col)
    local delta = {}
    local i = 0
    local gap = false
    for y = 1, b.height do
        if not gap and delta[i] and b[y][col] == delta[i].c then
            delta[i].n = delta[i].n + 1
            gap = false
        elseif b[y][col] ~= 0 then
            table.insert(delta, {c = b[y][col], n = 1})
            i = #delta
            gap = false
        else
            gap = true
        end
    end

    return delta
end

return board
