
--[[ loadgame.lua This state loads game.lua and presents a title screen.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local game = require('game')

local loadgame = {}

function loadgame.load(title, subtitle, level, color, palette, t, cont)
    state = {}
    state.name           = 'loadgame'
    state.update         = loadgame.update
    state.draw           = loadgame.draw
    state.mousepressed   = loadgame.mousepressed
    state.mousereleased  = loadgame.mousereleased
    state.mousemoved     = loadgame.mousemoved

    state.layout = {}
    state.layout.elements = {}

    loadgame.title = title
    loadgame.subtitle = subtitle
    loadgame.level = level
    loadgame.color = color
    loadgame.palette = palette
    loadgame.t = t
    loadgame.cont = cont
end

function loadgame.mousepressed(x, y, button)
    game.load(loadgame.level, loadgame.color, loadgame.palette,
              loadgame.t, loadgame.cont)
end

function loadgame.draw()
    love.graphics.setColor(0, 0, 0)
    love.graphics.setFont(layout.font_large)
    love.graphics.printf(loadgame.title, 0, love.graphics.getHeight() * 5/16,
                         love.graphics.getWidth(), 'center')
    if loadgame.subtitle then
        love.graphics.printf(loadgame.subtitle, 0, love.graphics.getHeight() * 7/16,
                             love.graphics.getWidth(), 'center')
    end
end

return loadgame
