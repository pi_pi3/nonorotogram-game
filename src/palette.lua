
--[[ palette.lua Color palette functionality used by this game.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local bit = require('bit') -- love version 10.2 still uses lua 5.1
local table = require('table')
local util = require('utils')
local gui = require('gui')
local json = require('dkjson')

local palette = {}

function palette.load(p)
    state = {}
    state.name           = 'palette'
    state.update         = palette.update
    state.draw           = palette.draw
    state.mousepressed   = palette.mousepressed
    state.mousereleased  = palette.mousereleased
    state.mousemoved     = palette.mousemoved
    state.textedit       = palette.textedit

    state.layout = {}
    state.layout.elements = layout.main.palette.elements

    palette.palette = util.deepcopy(p)
    palette.colorid = 1

    -- gui elements
    local scale = love.graphics.getWidth()/18

    -- color preview
    palette.preview_rect = gui.octagon(8*scale, 4*scale,
                                       layout.data.button.rx, layout.data.button.ry)
    palette.preview_text = love.graphics.newText(def_font, nil)
    palette.preview_text:addf(
        'Lorem ipsum dolor sit amet, semper petentium gubergren ea duo.\n' ..
        '012345  0 1 2 3 4 5',
        8*scale, 'left', 10*scale, 18.25*scale)

    -- color selection buttons
    layout.main.palette.elements.palettec = gui.container_scroll(
                                                4.5*scale, 0*scale, 14*scale, 3*scale,
                                                {lo = {x = nil, y = 0},
                                                 hi = {x = 0, y = 0}}, {},
                                                {r = 255, g = 255, b = 255},
                                                {r = 255, g = 255, b = 255}, true)
    local palettec = layout.main.palette.elements.palettec

    for i, c in ipairs(palette.palette) do
        table.insert(palettec.elements, palette.button(palettec, i, c))
    end

    palette.update_ui()
end

function palette.button(palettec, i, c)
    local scale = love.graphics.getWidth()/18

    local x = 5
    local y = 0.5
    local w = 2
    local h = 2

    local text = ''
    if options.colorblind then
        text = string.char(0x60+i)
        c = {r = 0, g = 0, b = 0}
    end
    return gui.button((x+(i-1)*w)*scale, y*scale, 
                      w*scale, h*scale,
                      c, text, 'center', nil, nil, nil, 
                      function()
                          palette.colorid = i
                          palette.update_ui()
                      end)
end

function palette.loadf(name, cache)
    if not cache and not util.endswith(name, '.json') then
        name = name .. '.json'
    end

    if not cache and not util.startswith(name, 'palette/') then
        name = 'palette/' .. name
    end

    local p_json = love.filesystem.read(name)
    local p = json.decode(p_json)

    for k, v in pairs(p) do
        if tonumber(k) then
            p[tonumber(k)] = v
            p[k] = nil
        end
    end

    if options.vibrate then
        love.system.vibrate(0.5)
    end

    return p
end

function palette.save(name, p, cache)
    p = util.default(p, palette.palette)
    if not name or #name == 0 then
        print('error: a name must be specified')
    end
    p.name = name

    if not cache and not util.endswith(name, '.json') then
        name = name .. '.json'
    end

    if not cache and util.startswith(name, 'cache/') then
        print('error: cannot save palette to cache')
    end

    if not cache and not util.startswith(name, 'palette/') then
        name = 'palette/' .. name
    end

    local p_str = util.map(p, function(e)
                                  return string.format('#%02x%02x%02x', e.r, e.g, e.b)
                              end)
    p_str.name = p.name
    local p_json = json.encode(p_str)

    if not love.filesystem.exists('palette/') then
        love.filesystem.createDirectory('palette')
    end

    local success = love.filesystem.write(name, p_json)
    if not success then
        print('error: couldn\'t save to file ' .. name) 
        return
    end

    if not cache then
        palette.update_customlist({name})
    end

    if options.vibrate then
        love.system.vibrate(0.5)
    end
end

function palette.mousemoved()
    palette.update_color()
end

function palette.mousepressed()
    palette.update_color()
end

function palette.mousereleased()
    palette.update_color()
end

function palette.update()
    if layout.main.palette.elements.text_hex.focus and not palette.editting then
        palette.editting = true
    elseif not layout.main.palette.elements.text_hex.focus and palette.editting then
        local color = palette.color(layout.main.palette.elements.text_hex.text)
        if color and color.r and color.g and color.b then
            if #palette.palette > 0 then
                palette.palette[palette.colorid] = color
            end
            palette.update_ui()
        end
        palette.editting = false
    end
end

function palette.update_ui()
    if #palette.palette == 0 then
        local black = {r = 0, g = 0, b = 0}
        -- update slider values
        layout.main.palette.elements.slider_r.value = black.r
        layout.main.palette.elements.slider_g.value = black.g
        layout.main.palette.elements.slider_b.value = black.b

        -- update label text
        layout.main.palette.elements.label_r.text = 'Red #00' 
        layout.main.palette.elements.label_g.text = 'Green #00' 
        layout.main.palette.elements.label_b.text = 'Blue #00' 

        -- update textfield text
        if not layout.main.palette.elements.text_hex.focus then
            layout.main.palette.elements.text_hex.text = '#000000'
        end

        return
    end

    local color = palette.palette[palette.colorid]

    -- update button color
    layout.main.palette.elements.palettec.elements[palette.colorid].bcolor = color

    -- update slider values
    layout.main.palette.elements.slider_r.value = color.r
    layout.main.palette.elements.slider_g.value = color.g
    layout.main.palette.elements.slider_b.value = color.b

    -- update label text
    layout.main.palette.elements.label_r.text = string.format('Red #%02x', 
                                          palette.palette[palette.colorid].r)
    layout.main.palette.elements.label_g.text = string.format('Green #%02x', 
                                         palette.palette[palette.colorid].g)
    layout.main.palette.elements.label_b.text = string.format('Blue #%02x', 
                                         palette.palette[palette.colorid].b)

    -- update textfield text
    if not layout.main.palette.elements.text_hex.focus then
        layout.main.palette.elements.text_hex.text = string.format('#%02x%02x%02x', 
                                              palette.palette[palette.colorid].r,
                                              palette.palette[palette.colorid].g,
                                              palette.palette[palette.colorid].b)
    end
end

function palette.update_color()
    if #palette.palette == 0 then
        layout.main.palette.elements.label_r.text = 'Red #00'
        layout.main.palette.elements.label_g.text = 'Green #00'
        layout.main.palette.elements.label_b.text = 'Blue #00'

        if not layout.main.palette.elements.text_hex.focus then
            layout.main.palette.elements.text_hex.text = '#000000'
        end

        return
    end

    palette.palette[palette.colorid].r = layout.main.palette.elements.slider_r.value
    palette.palette[palette.colorid].g = layout.main.palette.elements.slider_g.value
    palette.palette[palette.colorid].b = layout.main.palette.elements.slider_b.value

    layout.main.palette.elements.label_r.text = string.format('Red #%02x', 
                                          palette.palette[palette.colorid].r)
    layout.main.palette.elements.label_g.text = string.format('Green #%02x', 
                                         palette.palette[palette.colorid].g)
    layout.main.palette.elements.label_b.text = string.format('Blue #%02x', 
                                         palette.palette[palette.colorid].b)

    if not layout.main.palette.elements.text_hex.focus then
        layout.main.palette.elements.text_hex.text = string.format('#%02x%02x%02x', 
                                              palette.palette[palette.colorid].r,
                                              palette.palette[palette.colorid].g,
                                              palette.palette[palette.colorid].b)
    end
end

function palette.draw()
    local scale = love.graphics.getWidth()/18
    local color = palette.palette[palette.colorid] or {r = 0, g = 0, b = 0}
    love.graphics.setColor(color.r, color.g, color.b)

    love.graphics.draw(palette.preview_rect, 1*scale, 18*scale)
    love.graphics.draw(palette.preview_text)
end

function palette.color(x)
    local r
    local g
    local b

    if type(x) == 'string' then -- e.g. '#aabbcc' or 'aabbcc'
        local offset = 0
        if util.startswith(x, '#') then
            offset = 1
        end
        r = tonumber('0x' .. string.sub(x, 1+offset, 2+offset))
        g = tonumber('0x' .. string.sub(x, 3+offset, 4+offset))
        b = tonumber('0x' .. string.sub(x, 5+offset, 6+offset))
    elseif type(x) == 'int' then -- 0-8:r-8:g-8:b
        r = bit.rshift(bit.band(x, 0xff0000), 16)
        g = bit.rshift(bit.band(x, 0x00ff00), 8)
        b = bit.band(x, 0x0000ff)
    elseif type(x) == 'table' then
        if x.r and x.g and x.b then -- {r = r, g = g, b = b}
        -- the function could return x at this point
        -- instead a copy of the color is made
            r = x.r
            g = x.g
            b = x.b
        else -- {r, g, b}
            r = x[1]
            g = x[2]
            b = x[3]
        end
    end

    local color = {r = r, g = g, b = b}

    return color
end

function palette.gen(t)
    local p = {}
    p.name = t.name
    for _, c in ipairs(t) do
        table.insert(p, palette.color(c))
    end

    return p
end

function palette.update_customlist(p)
    if love.filesystem.exists('custom-palettes.prot') then
        local p_json, count = love.filesystem.read('custom-palettes.prot')
        if count == 0 then
            local p_json = json.encode({list = p})
            love.filesystem.write('custom-palettes.prot', p_json)
            return p
        else
            local p_current = json.decode(p_json)
            local p_new = util.concat(p_current.list, p)

            -- remove duplicates
            local hash = {}
            local p_nodup = {list = {}}
            for _, v in ipairs(p_new) do
                if not hash[v] then
                    table.insert(p_nodup.list, v)
                    hash[v] = true
                end
            end

            local p_json = json.encode(p_nodup)
            love.filesystem.write('custom-palettes.prot', p_json)
            return p_nodup.list
        end
    else
        local p_json = json.encode({list = p})
        love.filesystem.write('custom-palettes.prot', p_json)
        return p
    end
end

function palette.loadplist(normal)
    local p_list
    if love.filesystem.exists('custom-palettes.prot') then
        local p_json, count = love.filesystem.read('custom-palettes.prot')
        if count > 0 then
            p_list = json.decode(p_json).list
        end
    else
        p_list = {}
    end

    local ps = {}
    if normal then
        table.insert(ps, layout.palette.normal)
    end
    table.insert(ps, layout.palette.color)
    table.insert(ps, layout.palette.cga)

    for i, v in ipairs(p_list) do
        local p_json, count = love.filesystem.read(v)
        if count > 0 then
            local p = json.decode(p_json)
            local j = 1
            while p[tostring(j)]do
                p[j] = p[tostring(j)]
                p[tostring(j)] = nil
                j = j+1
            end

            p = palette.gen(p)

            table.insert(ps, p)
        end
    end

    return ps
end

function palette.genlist(p_list, x, y, w, h, mh, sx, sy)
    if not p_list then
        p_list = palette.loadplist()
    end

    local list = gui.list(x*sx, y*sy, w*sx, h*sy, mh*sy, {},
                          {r = 255, g = 255, b = 255})

    local elements = list.elements

    for i, v in ipairs(p_list) do
        elements[i] = {key = v.name, value = v}
    end

    list.selection = elements[1]

    return list
end

return palette
