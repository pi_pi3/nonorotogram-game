
--[[ layout.lua The UI layout used by this game.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local json = require('dkjson')
local menu = require('menu')
local board = require('board')
local game = require('game')
local loadgame = require('loadgame')
local editor = require('editor')
local palette = require('palette')

local version = 'public release\n1.0c'

return {
    main = {
        elements = {
            {t = "label", x = 0.125, y = 0.125, width = 16, height = 1,
             text = version, 
             color = {r = 0, g = 0, b = 0}},
            {t = "button", x = 1, y = 10, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Normal",
             onRelease = function() 
                state.layout.elements = layout.main.nlevels.elements
             end},
            {t = "button", x = 9, y = 10, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Color",
             onRelease = function() 
                state.layout.elements = layout.main.clevels.elements
             end},
            {t = "button", x = 5, y = 14, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Endless\nNormal",
             onRelease = function()
                state.layout.elements = layout.main.play.normal.elements
             end},
            {t = "button", x = 13, y = 14, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Endless\nColor",
             onRelease = function()
                state.layout.elements = layout.main.play.color.elements
             end},
            {t = "button", x = 1, y = 18, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Editor",
             onRelease = function() 
                state.layout.elements = layout.main.editor.elements
             end},
            {t = "button", x = 5, y = 22, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Custom levels",
             onRelease = function()
                state.layout.elements = layout.main.custom.elements
             end},
            {t = "button", x = 9, y = 18, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Options",
             onRelease = function() 
                state.layout.elements = layout.main.options.elements
             end},
            {t = "button", x = 13, y = 22, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Credits",
             onRelease = function() 
                state.layout.elements = layout.main.credits.elements
             end},
            {t = "label", x = 0, y = 2, width = 18, height = 1,
             text = "NoNo!\nRotoGram!", color = {r = 0, g = 0, b = 0},
             font = 'font_title', align = 'center'},
   escape = {t = "custom", onEscape = function()
                love.event.quit()
             end}
        },
        play = {
            elements = {},
            normal = {
                elements = {
         slider_1 = {t = "slider", x = 1, y = 3, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 3, max = 16, def = 4, round = true},
         slider_2 = {t = "slider", x = 1, y = 7, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 3, max = 16, def = 4, round = true},
         slider_3 = {t = "slider", x = 1, y = 11, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 1, max = 127, def = 31, round = true},
         slider_4 = {t = "slider", x = 1, y = 15, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 0.2, max = 0.8, def = 0.5, round = false},
                    {t = "label", x = 12, y = 3, width = 6, height = 1,
                     text = "Board width", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 7, width = 6, height = 1,
                     text = "Board height", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 11, width = 6, height = 1,
                     text = "Shuffles", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 15, width = 6, height = 1,
                     text = "Fill rate", 
                     color = {r = 0, g = 0, b = 0}},
           escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                     bcolor = {r = 0, g = 0, b = 0}, text = "Back",
                     onRelease = function()
                        state.layout.elements = layout.main.elements
                     end},
                    {t = "button", x = 9, y = 25, width = 8, height = 4,
                     bcolor = {r = 0, g = 0, b = 0}, text = "Play",
                     onRelease = function()
                        game.load(nil, false, nil, 'r')
                     end},
                    {t = "button", x = 11, y = 22.875, width = 6, height = 2,
                     bcolor = {r = 0, g = 0, b = 0}, text = "Continue",
                     onRelease = function()
                        game.load(nil, false, nil, 'r', true)
                     end}
                }
            },
            color = {
                elements = {
         slider_1 = {t = "slider", x = 1, y = 3, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 3, max = 16, def = 4, round = true},
         slider_2 = {t = "slider", x = 1, y = 7, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 3, max = 16, def = 4, round = true},
         slider_3 = {t = "slider", x = 1, y = 11, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 1, max = 127, def = 31, round = true},
         slider_4 = {t = "slider", x = 1, y = 15, width = 10, height = 1.5,
                     slider_width = 0.75,
                     min = 0.2, max = 1.0, def = 0.5, round = false},
                    {t = "label", x = 12, y = 3, width = 6, height = 1,
                     text = "Board width", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 7, width = 6, height = 1,
                     text = "Board height", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 11, width = 6, height = 1,
                     text = "Shuffles", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 15, width = 6, height = 1,
                     text = "Fill rate", 
                     color = {r = 0, g = 0, b = 0}},
                    {t = "label", x = 12, y = 19, width = 6, height = 1,
                     text = "Palette", 
                     color = {r = 0, g = 0, b = 0}},
           escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                     bcolor = {r = 0, g = 0, b = 0}, text = "Back",
                     onRelease = function()
                        state.layout.elements = layout.main.elements
                     end},
                    {t = "button", x = 9, y = 25, width = 8, height = 4,
                     bcolor = {r = 0, g = 0, b = 0}, text = "Play",
                     onRelease = function()
                        local palette_sel =
                            layout.main.play.color.elements.palette_list.selection.value
                        game.load(nil, true, palette_sel, 'r')
                     end},
                    {t = "button", x = 11, y = 22.875, width = 6, height = 2,
                     bcolor = {r = 0, g = 0, b = 0}, text = "Continue",
                     onRelease = function()
                        local palette_sel =
                            layout.main.play.color.elements.palette_list.selection.value
                        game.load(nil, true, palette_sel, 'r', true)
                     end}
                }
            }
        },
        editor = {
            elements = {
  textfield_1 = {t = "textfield", x = 1, y = 3, width = 10, height = 1,
                 fcolor = {r = 0, g = 0, b = 0},
                 bgcolor = {r = 255, g = 255, b = 255}},
     slider_1 = {t = "slider", x = 1, y = 7, width = 10, height = 1.5,
                 slider_width = 0.75,
                 min = 3, max = 16, def = 4, round = true},
     slider_2 = {t = "slider", x = 1, y = 11, width = 10, height = 1.5,
                 slider_width = 0.75,
                 min = 3, max = 16, def = 4, round = true},
   checkbox_1 = {t = "checkbox", x = 1, y = 15, width = 1.5, height = 1.5,
                 checked = false,
                 fcolor = {r = 0, g = 0, b = 0},
                 bgcolor = {r = 255, g = 255, b = 255}},
       edit_p = {t = "button", x = 1, y = 18.5, width = 2, height = 2,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Edit",
                 onRelease = function()
                    palette.load(layout.main.editor.elements.palette_list.selection.value)
                 end},
                {t = "label", x = 12, y = 3, width = 6, height = 1,
                 text = "Title", 
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 12, y = 7, width = 6, height = 1,
                 text = "Board width", 
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 12, y = 11, width = 6, height = 1,
                 text = "Board height", 
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 12, y = 15, width = 6, height = 1,
                 text = "Draw grid?", 
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 12, y = 19, width = 6, height = 1,
                 text = "Palette", 
                 color = {r = 0, g = 0, b = 0}},
       escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Back",
                 onRelease = function()
                    state.layout.elements = layout.main.elements
                 end},
                {t = "button", x = 9, y = 25, width = 8, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Edit",
                 onRelease = function()
                    local palette_sel =
                        layout.main.editor.elements.palette_list.selection.value
                    local color = #palette_sel > 1
                    editor.load(color, palette_sel)
                 end}
            }
        },
        options = {
            elements = {
                {t = "label", x = 7, y = 5, width = 6, height = 1,
                 text = "Colorblind",
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 7, y = 9, width = 6, height = 1,
                 text = "Vibrations",
                 color = {r = 0, g = 0, b = 0}},

                colorblind =
                {t = "checkbox", x = 3, y = 5, width = 1.5, height = 1.5,
                 checked = false,
                 fcolor = {r = 0, g = 0, b = 0},
                 bgcolor = {r = 255, g = 255, b = 255}},
                vibrate =
                {t = "checkbox", x = 3, y = 9, width = 1.5, height = 1.5,
                 checked = false,
                 fcolor = {r = 0, g = 0, b = 0},
                 bgcolor = {r = 255, g = 255, b = 255}},
                {t = "label", x = 0, y = 16, width = 18, height = 1,
                 text = "Scoreboard", font = 'font_big', align = 'center',
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 1, y = 18, width = 18, height = 1,
                 text = "Normal/randomized : ",
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 1, y = 19, width = 18, height = 1,
                 text = "Color/randomized : ",
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 1, y = 20, width = 18, height = 1,
                 text = "Normal/levels : ",
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 1, y = 21, width = 18, height = 1,
                 text = "Color/levels : ",
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 1, y = 22, width = 18, height = 1,
                 text = "Normal/custom levels : ",
                 color = {r = 0, g = 0, b = 0}},
                {t = "label", x = 1, y = 23, width = 18, height = 1,
                 text = "Color/custom levels : ",
                 color = {r = 0, g = 0, b = 0}},
       hi_r_n = {t = "label", x = 1, y = 18, width = 16, height = 1,
                 text = "n/a", align = 'right',
                 color = {r = 0, g = 0, b = 0}},
       hi_r_c = {t = "label", x = 1, y = 19, width = 16, height = 1,
                 text = "n/a", align = 'right',
                 color = {r = 0, g = 0, b = 0}},
       hi_l_n = {t = "label", x = 1, y = 20, width = 16, height = 1,
                 text = "n/a", align = 'right',
                 color = {r = 0, g = 0, b = 0}},
       hi_l_c = {t = "label", x = 1, y = 21, width = 16, height = 1,
                 text = "n/a", align = 'right',
                 color = {r = 0, g = 0, b = 0}},
       hi_c_n = {t = "label", x = 1, y = 22, width = 16, height = 1,
                 text = "n/a", align = 'right',
                 color = {r = 0, g = 0, b = 0}},
       hi_c_c = {t = "label", x = 1, y = 23, width = 16, height = 1,
                 text = "n/a", align = 'right',
                 color = {r = 0, g = 0, b = 0}},
                {t = "button", x = 11, y = 13, width = 6, height = 2,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Clear scoreboard",
                 onRelease = function()
                     love.filesystem.remove('hi/normal_r.prot')
                     love.filesystem.remove('hi/color_r.prot')
                     love.filesystem.remove('hi/normal_l.prot')
                     love.filesystem.remove('hi/color_l.prot')
                     love.filesystem.remove('hi/normal_c.prot')
                     love.filesystem.remove('hi/color_c.prot')
                     menu.loadhi()
                 end},
       escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Cancel",
                 onRelease = function()
                    state.layout.elements = layout.main.elements
                 end},
                {t = "button", x = 9, y = 25, width = 8, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Save",
                 onRelease = function()
                    local opts = {}
                    opts.colorblind = layout.main.options.elements.colorblind.checked
                    opts.vibrate = layout.main.options.elements.vibrate.checked

                    local opts_json = json.encode(opts)
                    love.filesystem.write('options.prot', opts_json)

                    options.colorblind = opts.colorblind
                    options.vibrate = opts.vibrate

                    state.layout.elements = layout.main.elements

                    if options.vibrate then
                        love.system.vibrate(0.5)
                    end
                 end}
            }
        },
        credits = {
            elements = {
                {t = "label", x = 0, y = 3, width = 18, height = 1, align = 'center',
                 text = "Design & code by\nSzymon Walter\na.k.a. \"pi_pi3\"" ..
                        "\n\n\n\n" ..
                        "Inspiration & dedication\nDagny Jaworska" ..
                        "\n\n\n\n" ..
                        "Original font (Akt) by\nEeve Somepx\n(@somepx)" ..
                        "\n\n\n" ..
                        "\n\n\n" ..
                        "Contact information\n" ..
                        "Twitter: @pi_pi314\n" ..
                        "itch.io: pi_pi3.itch.io\n" ..
                        "Blog: driftinginyellow.wordpress.com\n" ..
                        "E-mail: walter.szymon.98@gmail.com\n",
                 color = {r = 0, g = 0, b = 0}},
                {t = "button", x = 9, y = 25, width = 8, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "License",
                 onRelease = function()
                    state.layout.elements = layout.main.license.elements
                 end},
       escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Back",
                 onRelease = function()
                    state.layout.elements = layout.main.elements
                 end}
            }
        },
        license = {
            elements = {
                {t = "container.scroll", x = 1, y = 0.5, width = 16, height = 24,
                 bgcolor = {r = 255, g = 255, b = 255}, 
                 fcolor = {r = 255, g = 255, b = 255}, active = true,
                 scroll = {lo = {x = 0, y = nil}, hi = {x = 0, y = 0}},
                 elements = {
                    {t = "label", x = 0, y = 0, width = 16, height = 1, 
                     align = 'center',
                     text = love.filesystem.read('LICENSE'), 
                     color = {r = 0, g = 0, b = 0}}
                 }
                },
                {t = "button", x = 9, y = 25, width = 8, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Credits",
                 onRelease = function()
                    state.layout.elements = layout.main.credits.elements
                 end},
       escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Back",
                 onRelease = function()
                    state.layout.elements = layout.main.elements
                 end}
            }
        },
        palette = {
            elements = {
    slider_r = {t = "slider", x = 1, y = 6, width = 10, height = 1.5,
                slider_width = 0.75,
                min = 0, max = 255, def = 0, round = true},
    slider_g = {t = "slider", x = 1, y = 10, width = 10, height = 1.5,
                slider_width = 0.75,
                min = 0, max = 255, def = 0, round = true},
    slider_b = {t = "slider", x = 1, y = 14, width = 10, height = 1.5,
                slider_width = 0.75,
                min = 0, max = 255, def = 2, round = true},
     label_r = {t = "label", x = 12, y = 6, width = 6, height = 1,
                text = "Red", color = {r = 0, g = 0, b = 0}},
     label_g = {t = "label", x = 12, y = 10, width = 6, height = 1,
                text = "Green", color = {r = 0, g = 0, b = 0}},
     label_b = {t = "label", x = 12, y = 14, width = 6, height = 1,
                text = "Blue", color = {r = 0, g = 0, b = 0}},
    text_hex = {t = "textfield", x = 3, y = 16.5, width = 4, height = 1,
                fcolor = {r = 0, g = 0, b = 0},
                bgcolor = {r = 255, g = 255, b = 255}},
  button_new = {t = "button", x = 0.5, y = 0.5, width = 3.5, height = 3.5,
                bcolor = {r = 0, g = 0, b = 0}, text = "(+)", font = 'font_big',
                onRelease = function() 
                    local color = {}
                    color.r = math.random(64, 192)
                    color.g = math.random(64, 192)
                    color.b = math.random(64, 192)
                    local i = #palette.palette+1
                    local palettec = layout.main.palette.elements.palettec
                    table.insert(palettec.elements, palette.button(palettec, i, color))
                    palette.palette[i] = color
                    palette.colorid = i
                    palette.update_ui()
                end},
  button_del = {t = "button", x = 15, y = 3, width = 2, height = 2,
                bcolor = {r = 0, g = 0, b = 0}, text = "(-)",
                onRelease = function() 
                    local i = palette.colorid
                    table.remove(palette.palette, i)
                    table.remove(layout.main.palette.elements.palettec.elements, i)

                    palette.colorid = math.min(palette.colorid, #palette.palette)

                    -- reposition color selection buttons
                    local scale = love.graphics.getWidth()/18
                    for i = i, #layout.main.palette.elements.palettec.elements do
                        local b = layout.main.palette.elements.palettec.elements[i]
                        b.x = b.x - 2*scale
                        -- the onRelease function has to be changed for the new colorid
                        b.onRelease = function()
                            palette.colorid = i
                            palette.update_ui()
                        end
                    end
                    palette.update_ui()
                end},
      escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                bcolor = {r = 0, g = 0, b = 0}, text = "Cancel",
                onRelease = function()
                    menu.load()
                    state.layout.elements = layout.main.editor.elements
                end},
               {t = "button", x = 9, y = 25, width = 8, height = 4,
                bcolor = {r = 0, g = 0, b = 0}, text = "Save",
                onRelease = function()
                    state.pause = true
                    layout.main.palette.elements.save.active = true
                end},
        save = {t = "container", x = 2, y = 10, width = 14, height = 8, -- save
                bgcolor = {r = 255, g = 255, b = 255}, active = false,
                elements = {
            name = {t = "textfield", x = 1, y = 1, width = 12, height = 1,
                    fcolor = {r = 0, g = 0, b = 0},
                    bgcolor = {r = 255, g = 255, b = 255},
                    editable = true},
                   {t = "button", x = 1, y = 3, width = 4, height = 4,
                    bcolor = {r = 0, g = 0, b = 0}, text = "Cancel",
                    onRelease = function()
                       layout.main.palette.elements.save.active = false
                       state.pause = false
                    end},
                   {t = "button", x = 9, y = 3, width = 4, height = 4,
                    bcolor = {r = 0, g = 0, b = 0}, text = "Ok",
                    onRelease = function()
                       layout.main.palette.elements.save.active = false
                       state.pause = false
                       palette.save(
                           layout.main.palette.elements.save.elements.name.text)
                        menu.load()
                        state.layout.elements = layout.main.editor.elements
                    end}
                },
               }
           }
        }
    },
    game = {
        elements = {
   escape = {t = "button", x = 0.5, y = 0.5, width = 3.5, height = 3.5,
             bcolor = {r = 0, g = 0, b = 0}, text = "(*)", font = 'font_big',
             onRelease = function() 
                layout.game.elements[1].active = not layout.game.elements[1].active
                state.pause = not state.pause
             end},
            {t = "container", x = 3, y = 4, width = 12, height = 20,
             bgcolor = {r = 255, g = 255, b = 255}, active = false,
             elements = {
                {t = "button", x = 2, y = 2, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Main menu",
                 onRelease = function()
                    menu.load()
                 end},
                {t = "button", x = 6, y = 6, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Continue",
                 onRelease = function()
                    layout.game.elements[1].active = false
                    state.pause = false
                 end},
                {t = "button", x = 2, y = 10, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Save & quit",
                 onRelease = function()
                    game.save()
                    menu.load()
                 end},
                {t = "button", x = 6, y = 14, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Reset",
                 onRelease = function()
                    game.reset()
                    layout.game.elements[1].active = false
                    state.pause = false
                 end}
             }
            }
        },
        board = {
            t = "custom",
            x = 0, y = 5, width = 16, height = 16,
            color = {r = 255, g = 255, b = 255},
            sep = 0.2666, round = 0.2666,
            text_top = {x = 0, y = 8},
            text_side = {x = 0, y = 8}
        },
        over = {
            elements = {
       escape = {t = "button", x = 1, y = 25, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Back",
                 onRelease = function()
                    menu.load()
                 end},
                {t = "button", x = 9, y = 25, width = 8, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Next",
                 onRelease = function()
                    if game.t == 'l' then
                        local next_lvl
                        if game.level[1] == 'n' then
                            next_lvl = levels.normal[game.index+1]
                        elseif game.level[1] == 'c' then
                            next_lvl = levels.color[game.index+1]
                        end

                        local title
                        local subtitle
                        if next_lvl then
                            title = game.index+1
                            subtitle = next_lvl.name
                        end

                        local donext = next_lvl ~= nil
                        next_lvl = {game.level[1], game.level[2]+1}
                        if donext then
                            loadgame.load(title, subtitle, next_lvl, nil, nil, 'l')
                        end
                    elseif game.level == 'random' then
                        game.load(nil, game.colors, game.palette, 'r')
                    else
                        menu.load()
                    end
                 end}
            },
        }
    },
    editor = {
        elements = {
   escape = {t = "button", x = 0.5, y = 0.5, width = 3.5, height = 3.5,
             bcolor = {r = 0, g = 0, b = 0}, text = "(*)", font = 'font_big',
             onRelease = function() 
                layout.editor.elements[1].active = true
                state.pause = true
             end},
   edit_s = {t = "button", x = 13, y = 25, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Edit shuffled",
             onRelease = function() 
                if not editor.shuffled then
                    editor.shuffled =
                        board.new(editor.board.width, editor.board.height, 0, 1)
                end
                editor.edited = editor.shuffled
             end},
   edit_b = {t = "button", x = 1, y = 25, width = 4, height = 4,
             bcolor = {r = 0, g = 0, b = 0}, text = "Edit board",
             onRelease = function() 
                editor.edited = editor.board
             end},
            {t = "container", x = 3, y = 4, width = 12, height = 20,
             bgcolor = {r = 255, g = 255, b = 255}, active = false,
             elements = {
                {t = "button", x = 2, y = 2, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Main menu",
                 onRelease = function()
                    menu.load()
                 end},
       escape = {t = "button", x = 6, y = 6, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Continue",
                 onRelease = function()
                    layout.editor.elements[1].active = false
                    state.pause = false
                 end},
                {t = "button", x = 2, y = 10, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Save",
                 onRelease = function()
                    layout.editor.elements[3].active = true
                    layout.editor.elements[3].elements[1].text = editor.name
                    layout.editor.elements[1].pause = true
                 end},
                {t = "button", x = 6, y = 14, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Load",
                 onRelease = function()
                    layout.editor.elements[2].active = true
                    layout.editor.elements[1].pause = true
                 end}
             }
            },
            {t = "container", x = 2, y = 10, width = 14, height = 8, -- load file
             bgcolor = {r = 255, g = 255, b = 255}, active = false,
             elements = {
                {t = "textfield", x = 1, y = 1, width = 12, height = 1,
                 fcolor = {r = 0, g = 0, b = 0},
                 bgcolor = {r = 255, g = 255, b = 255}},
       escape = {t = "button", x = 1, y = 3, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Cancel",
                 onRelease = function()
                    layout.editor.elements[2].active = false
                    layout.editor.elements[1].pause = false
                 end},
                {t = "button", x = 9, y = 3, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Ok",
                 onRelease = function()
                    layout.editor.elements[2].active = false
                    layout.editor.elements[1].pause = false
                    editor.loadf(layout.editor.elements[2].elements[1].text)
                 end}
             }
            },
            {t = "container", x = 2, y = 10, width = 14, height = 8, -- save file
             bgcolor = {r = 255, g = 255, b = 255}, active = false,
             elements = {
                {t = "textfield", x = 1, y = 1, width = 12, height = 1,
                 fcolor = {r = 0, g = 0, b = 0},
                 bgcolor = {r = 255, g = 255, b = 255}},
       escape = {t = "button", x = 1, y = 3, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Cancel",
                 onRelease = function()
                    layout.editor.elements[3].active = false
                    layout.editor.elements[1].pause = false
                 end},
                {t = "button", x = 9, y = 3, width = 4, height = 4,
                 bcolor = {r = 0, g = 0, b = 0}, text = "Ok",
                 onRelease = function()
                    layout.editor.elements[3].active = false
                    layout.editor.elements[1].pause = false
                    editor.save(layout.editor.elements[3].elements[1].text)
                 end}
             },
            },
    clear = {t = "button", x = 11, y = 3, width = 6, height = 2,
             bcolor = {r = 0, g = 0, b = 0}, text = "Clear",
             onRelease = function() 
                editor.clear()
             end}
        },
        board = {
            t = "custom",
            x = 0, y = 5, width = 16, height = 16,
            color = {r = 255, g = 255, b = 255},
            sep = 0.2666, round = 0.2666,
            text_top = {x = 0, y = 8},
            text_side = {x = 0, y = 8}
        }
    },
    data = {
        font = {
            font_normal = {
                path = "fonts/Akt_smooth.ttf",
                ttf = true,
                size = 0.6666,
                format = nil
            },
            font_big = {
                path = "fonts/Akt_smooth.ttf",
                ttf = true,
                size = 1.3333,
                format = nil
            },
            font_large = {
                path = "fonts/Akt_smooth.ttf",
                ttf = true,
                size = 2.0,
                format = nil
            },
            font_title = {
                path = "fonts/Akt_smooth.ttf",
                ttf = true,
                size = 2.6666,
                format = nil
            }
        },
        button = {
            rx = 0.2666,
            ry = 0.2666
        },
        slider = {
            rx = 0.2,
            ry = 0.2,
            line_width = 0.06
        },
        checkbox = {
            rx = 0.1333,
            ry = 0.1333,
            line_width = 0.06
        },
        textfield = {
            rx = 0.1333,
            ry = 0.1333,
            line_width = 0.06
        },
        container = {
            rx = 0.2666,
            ry = 0.2666,
            line_width = 0.06
        },
        list = {
            rx = 0.1333,
            ry = 0.1333,
            line_width = 0.06
        }
    },
    palette = {
        normal = {
            name = 'Black n\' black',
            {r = 0, g = 0, b = 0}
        },
        color = {
            name = 'Pastel',
            '#d34747', -- red
            '#39538d', -- green
            '#39a939', -- blue
            '#d3d347', -- yellow
            '#a93978', -- purple
            '#2b7f7f'  -- cyan
        },
        cga = {
            name = 'CGA',
            '#000000', -- black
            '#aa0000', -- red
            '#00aa00', -- green
            '#0000aa', -- blue
            '#aa5500', -- brown
            '#00aaaa', -- cyan
            '#aa00aa', -- magenta
            '#aaaaaa', -- light gray
            '#555555', -- dark gray
            '#ff5555', -- bright red
            '#55ff55', -- bright green
            '#5555ff', -- bright blue
            '#ffff55', -- bright yellow
            '#55ffff', -- bright cyan
            '#ff55ff'  -- bright magenta
        }
    }
}

