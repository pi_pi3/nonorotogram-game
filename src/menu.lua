
--[[ menu.lua The main menu state.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local palette = require('palette')
local level = require('level')
local game = require('game')

local menu = {}

function menu.load()
    state = {}
    state.name           = 'menu'
    state.update         = menu.update
    state.draw           = menu.draw
    state.mousepressed   = menu.mousepressed
    state.mousereleased  = menu.mousereleased
    state.mousemoved     = menu.mousemoved

    state.layout = {}
    state.layout.elements = layout.main.elements

    -- palette lists
    local scale = love.graphics.getWidth()/18

    layout.main.editor.elements.palette_list =
        palette.genlist(palette.loadplist(true), 4, 19, 7, 1, 4, scale, scale)

    layout.main.play.color.elements.palette_list =
        palette.genlist(palette.loadplist(false), 1, 19, 10, 1, 4, scale, scale)

    -- custom levels
    layout.main.custom = level.load_custom(scale, scale)

    menu.loadhi()
end

function menu.loadhi()
    -- highscore board
    local inf = 1e309
    local hi_r_n = game.loadhi('hi/normal_r.prot')
    local hi_r_c = game.loadhi('hi/color_r.prot')
    local hi_l_n = game.loadhi('hi/normal_l.prot')
    local hi_l_c = game.loadhi('hi/color_l.prot')
    local hi_c_n = game.loadhi('hi/normal_c.prot')
    local hi_c_c = game.loadhi('hi/color_c.prot')
    -- de-init highscore
    game.highscore = nil

    if hi_r_n ~= -inf then
        layout.main.options.elements.hi_r_n.text = tostring(hi_r_n)
    else
        layout.main.options.elements.hi_r_n.text = 'n/a'
    end

    if hi_r_c ~= -inf then
        layout.main.options.elements.hi_r_c.text = tostring(hi_r_c)
    else
        layout.main.options.elements.hi_r_c.text = 'n/a'
    end

    if hi_l_n ~= -inf then
        layout.main.options.elements.hi_l_n.text = tostring(hi_l_n)
    else
        layout.main.options.elements.hi_l_n.text = 'n/a'
    end

    if hi_l_c ~= -inf then
        layout.main.options.elements.hi_l_c.text = tostring(hi_l_c)
    else
        layout.main.options.elements.hi_l_c.text = 'n/a'
    end

    if hi_c_n ~= -inf then
        layout.main.options.elements.hi_c_n.text = tostring(hi_c_n)
    else
        layout.main.options.elements.hi_c_n.text = 'n/a'
    end

    if hi_c_c ~= -inf then
        layout.main.options.elements.hi_c_c.text = tostring(hi_c_c)
    else
        layout.main.options.elements.hi_c_c.text = 'n/a'
    end
end

return menu
