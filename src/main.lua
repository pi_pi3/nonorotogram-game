
--[[ main.lua The entry point for this game. Execution starts at love.load()
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local table = require('table')
local util = require('utils')
local gui = require('gui')
local menu = require('menu')
local level = require('level')
local palette = require('palette')
local json = require('dkjson')
local game = require('game')
local editor = require('editor')

function love.load()
    util.init_G()

    util.declare('state', {})
    util.declare('layout', {})
    util.declare('levels', {})
    util.declare('options', {})

    options.colorblind = false -- range: false/true
    options.vibrate = true -- range: false/true

    if love.filesystem.exists('options.prot') then
        local opts_json = love.filesystem.read('options.prot')
        local opts = json.decode(opts_json)
        options.colorblind = opts.colorblind
        options.vibrate = opts.vibrate
    end

    math.randomseed(os.time())
    love.mouse.setVisible(true)
    love.keyboard.setKeyRepeat(true)
    love.keyboard.setTextInput(false)

    -- layout
    local base = {x = 1/18, y = 1/32}
    local w, h = love.graphics.getWidth(), love.graphics.getHeight()
    local scale = base.x*w
    local l = require('layout')

    -- font
    local data = l.data
    for k, font in pairs(data.font) do
        font.size = math.floor(font.size*scale+0.5)
        if font.path and love.filesystem.exists(font.path) then
            if font.ttf then
                font = love.graphics.newFont(font.path, font.size)
            else
                font = love.graphics.newImageFont(font.path, font.format)
            end
        else
            font = love.graphics.newFont(font.size)
        end
        font:setFilter('nearest', 'nearest')
        layout[k] = font
    end
    data.button.rx = math.floor(data.button.rx*scale+0.5)
    data.button.ry = math.floor(data.button.ry*scale+0.5)
    data.slider.rx = math.floor(data.slider.rx*scale+0.5)
    data.slider.ry = math.floor(data.slider.ry*scale+0.5)
    data.slider.line_width = math.floor(data.slider.line_width*scale+0.5)
    data.checkbox.rx = math.floor(data.checkbox.rx*scale+0.5)
    data.checkbox.ry = math.floor(data.checkbox.ry*scale+0.5)
    data.checkbox.line_width = math.floor(data.checkbox.line_width*scale+0.5)
    data.textfield.rx = math.floor(data.textfield.rx*scale+0.5)
    data.textfield.ry = math.floor(data.textfield.ry*scale+0.5)
    data.textfield.line_width = math.floor(data.textfield.line_width*scale+0.5)
    data.container.rx = math.floor(data.container.rx*scale+0.5)
    data.container.ry = math.floor(data.container.ry*scale+0.5)
    data.container.line_width = math.floor(data.container.line_width*scale+0.5)
    data.list.rx = math.floor(data.list.rx*scale+0.5)
    data.list.ry = math.floor(data.list.ry*scale+0.5)
    data.list.line_width = math.floor(data.list.line_width*scale+0.5)
    layout.data = data

    util.declare('def_font', layout.font_normal)
    love.graphics.setFont(def_font)

    -- init game n' stuff
    layout.main = gui.decode(l.main, scale, scale)
    layout.game = gui.decode(l.game, scale, scale)
    layout.editor = gui.decode(l.editor, scale, scale)
    layout.palette = {}
    for k, p in pairs(l.palette) do
        layout.palette[k] = palette.gen(p)
    end

    -- set options menu to correct values
    local opts = layout.main.options.elements
    opts.colorblind.checked = options.colorblind
    opts.vibrate.checked = options.vibrate

    -- scaling
    layout.game.board.sep = math.floor(layout.game.board.sep*scale+0.5)
    layout.game.board.round = math.floor(layout.game.board.round*scale+0.5)

    layout.game.board.text_top.x = math.floor(layout.game.board.text_top.x*scale+0.5)
    layout.game.board.text_top.y = math.floor(layout.game.board.text_top.y*scale+0.5)
    layout.game.board.text_side.x = math.floor(layout.game.board.text_side.x*scale+0.5)
    layout.game.board.text_side.y = math.floor(layout.game.board.text_side.y*scale+0.5)

    layout.editor.board.sep = math.floor(layout.editor.board.sep*scale+0.5)
    layout.editor.board.round = math.floor(layout.editor.board.round*scale+0.5)

    layout.editor.board.text_top.x = math.floor(
                                        layout.editor.board.text_top.x*scale+0.5)
    layout.editor.board.text_top.y = math.floor(
                                        layout.editor.board.text_top.y*scale+0.5)
    layout.editor.board.text_side.x = math.floor(
                                        layout.editor.board.text_side.x*scale+0.5)
    layout.editor.board.text_side.y = math.floor(
                                        layout.editor.board.text_side.y*scale+0.5)

    -- levels
    levels.normal = level.load('n')
    levels.color = level.load('c')
    layout.main.nlevels = level.guigen(levels.normal, scale, scale, 'n')
    layout.main.clevels = level.guigen(levels.color, scale, scale, 'c')

    if love.filesystem.exists('cache/savefile_cached.prot') then
        game.load()
        game.loadf('cached', true)
        love.filesystem.remove('cache/savefile_cached.prot')
    elseif love.filesystem.exists('cache/editor_cached.prot') then
        editor.load()
        editor.loadf('cache/editor_cached.prot', true)
        love.filesystem.remove('cache/editor_cached.prot')
    else
        menu.load()
    end
end

function love.quit()
    autosave()
end

function love.mousepressed(mx, my, button)
    if not state.pause then
        if state.mousepressed then state.mousepressed(mx, my, button) end
    end

    if button == 1 then
        gui.mousepressed(state.layout.elements, mx, my)
    end
end

function love.mousereleased(mx, my, button)
    if not state.pause then
        if state.mousereleased then state.mousereleased(mx, my, button) end
    end

    if button == 1 then
        gui.mousereleased(state.layout.elements, mx, my)
    end
end

function love.mousemoved(mx, my, dx, dy)
    if not state.pause then
        if state.mousemoved then state.mousemoved(mx, my, dx, dy) end
    end

    gui.mousemoved(state.layout.elements, mx, my, dx, dy)
end

function love.wheelmoved(dx, dy)
    if not state.pause then
        if state.wheelmoved then state.wheelmoved(dx, dy) end
    end

    gui.wheelmoved(state.layout.elements, dx, dy)
end

function love.textinput(c)
    if not state.pause then
        if state.textinput then state.textinput(mx, my, dx, dy) end
    end

    gui.textinput(state.layout.elements, c)
end

function love.keypressed(key, scancode, isrepeat)
    if not state.pause then
        if state.keypressed then state.keypressed(key, scancode, isrepeat) end
    end

    gui.keypressed(state.layout.elements, key, scancode, isrepeat)
end

function love.focus(focus)
    autopause(not focus)
    if not focus then
        autosave()
    end
end

function love.visible(visible)
    autopause(not visible)
    if not visible then
        autosave()
    end
end

function love.update(dt)
    if not state.pause then
        if state.update then state.update(dt) end

        gui.update(state.layout.elements, dt)
    end
end

function love.draw()
    love.graphics.clear(255, 255, 255)

    if state.draw then state.draw() end

    if state.layout and state.layout.elements then
        gui.drawall(state.layout.elements)
    end
end

function autosave()
    if state.name == 'game' then
        game.save('cached', true)
    elseif state.name == 'editor' then
        editor.save('cache/editor_cached.prot', true)
    end
end

function autopause(pause)
    state.pause = pause
end

