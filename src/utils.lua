
--[[ utils.lua A minimalistic util library useful for games.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local table = require('table')

local util = {}

-- declare a global variable
-- declare('x', 1)
function util.declare(name, val) rawset(_G, name, val or false)
end

-- this function makes implicit global declarations cause a runtime error
-- for safety reasons
-- and typos
-- it makes life easier
function util.init_G()
    setmetatable(_G, {
        __newindex = function (_, n)
            error("write to undeclared variable " .. n, 2)
        end,
        __index = function (_, n)
            error("read from undeclared variable " .. n, 2)
        end,
    })
end

function util.default(var, def)
    return (var == nil) and def or var
end

-- naive method
function util.table_tostr(t, b, e, sep)
    if not t then 
        return nil 
    end
    b = util.default(b, '{')
    e = util.default(e, '}')
    sep = util.default(sep, ', ')
    local str = b
    for k, v in pairs(t) do
        if str ~= b then
            str = str .. sep
        end

        if type(k) == 'string' then
            str = str .. k .. ' = '
        end

        if type(v) == 'table' then
            str = str .. util.table_tostr(v, b, e, sep)
        elseif type(v) == 'string' then
            str = str .. '"' .. v .. '"' 
        else
            str = str .. tostring(v)
        end
    end
    str = str .. e
    return str
end

function util.cmp_table(t1, t2)
    for k, e1 in pairs(t1) do
        local e2 = t2[k]
        if type(e1) == 'table' and
           type(e2) == 'table' then
            if not util.cmp_table(e1, e2) then
                return false
            end
        else
            if e1 ~= e2 then
                return false
            end
        end
    end
    return true
end

function util.deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[util.deepcopy(orig_key)] = util.deepcopy(orig_value)
        end
        setmetatable(copy, util.deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function util.fst(x, y)
    return x
end

function util.snd(x, y)
    return y
end

function util.clamp(x, min, max)
    if min == nil then
        if max == nil then
            return x
        else
            return math.min(x, max)
        end
    end

    if max == nil then
        return math.max(x, min)
    end

    return math.min(max, math.max(x, min))
end

function util.match(text, pattern)
    local out = {}
    for v in string.gmatch(text, pattern) do
        table.insert(out, v)
    end
    return out
end

function util.between(a, min, max)
    if max == nil and min == nil then
        return true
    elseif max == nil then
        return (a >= min)
    elseif min == nil then
        return (a <= max)
    end
    return (a >= min) and (a <= max)
end

function util.endswith(a, b)
    return string.sub(a, #a-#b+1, #a) == b
end

function util.startswith(a, b)
    return string.sub(a, 1, #b) == b
end

function util.concat(a, b)
    local t = {}
    for i, v in ipairs(a) do
        t[i] = v
    end
    for i, v in ipairs(b) do
        t[#t+i] = v
    end

    return t
end

function util.map(t, fn)
    local new = {}
    for i, v in ipairs(t) do
        new[i] = fn(v)
    end
    return new
end

return util
