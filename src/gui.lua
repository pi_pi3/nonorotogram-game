
--[[ gui.lua A minimalistic GUI library useful for games.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local math = require('math')
local util = require('utils')

local gui = {}

function gui.octagon(width, height, rx, ry, mode)
    rx = math.min(rx, width/2)
    ry = math.min(ry, height/2)
    if not mode or mode == 'fill' then 
        local polygon = {{0,          ry},
                         {0,          height-ry},
                         {rx,         height},
                         {width-rx,   height},
                         {width,      height-ry},
                         {width,      ry},
                         {width-rx,   0},
                         {rx,         0}}
        return love.graphics.newMesh(polygon, 'fan', 'static')
    elseif mode == 'line' then
        return {0,        ry,
                0,        height-ry,
                rx,       height,
                width-rx, height,
                width,    height-ry,
                width,    ry,
                width-rx, 0,
                rx,       0,
                0,        ry}
    end
end

function gui.drawoctagon(mesh, x, y, color, mode, linewidth)
    if not mode or mode == 'fill' then
        love.graphics.setColor(color.r, color.g, color.b)
        love.graphics.draw(mesh, x, y)
    elseif mode == 'line' then
        love.graphics.setLineStyle('smooth')
        love.graphics.setLineWidth(linewidth)
        love.graphics.setColor(color.r, color.g, color.b)
        love.graphics.translate(x, y)
        love.graphics.line(mesh)
        love.graphics.translate(-x, -y)
    end
end

-- TODO add Text object
function gui.label(x, y, width, height, text, align, font, color, onHover)
    local label = {}

    label.t = 'label'
    label.onHover = onHover
    label.text = text
    label.align = util.default(align, 'left')
    label.font = util.default(font, def_font)
    label.color = color
    label.x = x
    label.y = y
    label.width = width
    label.height = util.default(height, util.fst(label.font:getWrap(text, width)))

    return label
end

function gui.image(x, y, width, height, r, img, onHover)
    local image = {}

    image.t = 'image'
    image.onHover = onHover
    image.img = img
    image.x = x
    image.y = y
    image.r = util.default(r, 0)
    image.width = util.default(width, img.getWidth())
    image.height = util.default(height, img.getHeight())
    image.quad = love.graphics.newQuad(0, 0, image.width, image.height, 
                                       img.getWidth(), img.getHeight())

    return image
end

function gui.button(x, y, width, height, bcolor, text, align, font, tcolor,
                    onClick, onRelease, onHover, onHold)
    local button = {}

    button.t = 'button'
    button.onClick = onClick
    button.onRelease = onRelease
    button.onHover = onHover
    button.onHold = onHold
    button.text = text
    button.align = util.default(align, 'center')
    button.font = util.default(font, def_font)
    button.bcolor = bcolor
    button.tcolor = util.default(tcolor, 
                                {r=255-bcolor.r, b=255-bcolor.g, g=255-bcolor.b})
    button.x = x
    button.y = y
    button.width = width
    button.height = height
    button.down = false

    local rx = layout.data.button.rx
    local ry = layout.data.button.ry

    button.mesh = gui.octagon(button.width, button.height, rx, ry)

    return button
end

function gui.slider(x, y, width, height, slider_width, min, max, def, round,
                    onClick, onRelease, onHover, onHold)
    local slider = {}

    slider.t = 'slider'
    slider.onClick = onClick
    slider.onRelease = onRelease
    slider.onHover = onHover
    slider.onHold = onHold

    slider.min = util.default(min, 0)
    slider.max = util.default(max, 1)
    slider.value = util.default(def, slider.min)
    slider.round = util.default(round, false)
    slider.slider_width = slider_width
    slider.x = x
    slider.y = y
    slider.width = width
    slider.height = height
    slider.down = false

    local rx = layout.data.slider.rx
    local ry = layout.data.slider.ry

    slider.left = {rx,       0,
                   0,        ry,
                   0,        height-ry,
                   rx,       height}
    slider.right = {width-rx, 0,
                    width,    ry,
                    width,    height-ry,
                    width-rx, height}
    slider.mid = {0, height/2, width, height/2}

    local sw, sh = slider.slider_width, slider.slider_width
    slider.slider = love.graphics.newMesh(
                        {{-sw/2, 0},
                         {0, sh/2},
                         {sw/2, 0},
                         {0, -sh/2}})

    return slider
end

function gui.checkbox(x, y, width, height, checked, bgcolor, fcolor,
                      onClick, onRelease, onHover, onHold)
    local checkbox = {}

    checkbox.t = 'checkbox'
    checkbox.onClick = onClick
    checkbox.onRelease = onRelease
    checkbox.onHover = onHover
    checkbox.onHold = onHold
    checkbox.bgcolor = bgcolor
    checkbox.fcolor = fcolor

    checkbox.x = x
    checkbox.y = y
    checkbox.width = width
    checkbox.height = height
    checkbox.checked = util.default(checked, false)

    local rx = layout.data.checkbox.rx
    local ry = layout.data.checkbox.ry

    checkbox.mesh = gui.octagon(checkbox.width, checkbox.height, rx, ry)
    checkbox.lines = gui.octagon(checkbox.width, checkbox.height, rx, ry, 'line')

    return checkbox
end

function gui.container(x, y, width, height, elements, bgcolor, fcolor, active)
    local container = {}

    container.t = 'container'
    container.elements = elements
    container.bgcolor = bgcolor
    container.fcolor = util.default(fcolor, 
                                {r=255-bgcolor.r, b=255-bgcolor.g, g=255-bgcolor.b})
    container.x = x
    container.y = y
    container.width = width
    container.height = height
    container.active = util.default(active, true)

    local rx = layout.data.container.rx
    local ry = layout.data.container.ry

    container.mesh = gui.octagon(container.width, container.height, rx, ry)
    container.lines = gui.octagon(container.width, container.height, rx, ry, 'line')

    return container
end

function gui.textfield(x, y, width, height, text, font, editable,
                       bgcolor, fcolor, onHover)
    local textfield = {}

    textfield.t = 'textfield'
    textfield.font = util.default(font, def_font)
    textfield.text = util.default(text, '')
    textfield.editable = util.default(editable, true)
    textfield.focus = false
    textfield.bgcolor = bgcolor
    textfield.fcolor = util.default(fcolor, 
                                {r=255-bgcolor.r, b=255-bgcolor.g, g=255-bgcolor.b})
    textfield.x = x
    textfield.y = y
    textfield.width = width
    textfield.height = height
    textfield.onHover = onHover

    local rx = layout.data.textfield.rx
    local ry = layout.data.textfield.ry

    textfield.mesh = gui.octagon(textfield.width, textfield.height, rx, ry)
    textfield.lines = gui.octagon(textfield.width, textfield.height, rx, ry, 'line')

    return textfield
end

function gui.container_scroll(x, y, width, height, scroll, elements, 
                              bgcolor, fcolor, active)
    local container = {}

    container.t = 'container.scroll'
    container.elements = elements
    container.bgcolor = bgcolor
    container.fcolor = util.default(fcolor, 
                                {r=255-bgcolor.r, b=255-bgcolor.g, g=255-bgcolor.b})
    container.x = x
    container.y = y
    container.width = width
    container.height = height
    container.active = util.default(active, true)
    container.maxscroll = scroll
    container.scroll = {x = 0, y = 0}
    container.down = false

    local rx = layout.data.container.rx
    local ry = layout.data.container.ry

    container.mesh = gui.octagon(container.width, container.height, rx, ry)
    container.lines = gui.octagon(container.width, container.height, rx, ry, 'line')

    return container
end

function gui.list(x, y, width, height, max_height, elements, bgcolor, fcolor, font)
    local list = {}

    list.t = 'list'
    list.elements = elements
    list.selection = elements[1]
    list.font = util.default(font, def_font)
    list.bgcolor = bgcolor
    list.fcolor = util.default(fcolor, 
                                {r=255-bgcolor.r, b=255-bgcolor.g, g=255-bgcolor.b})
    list.x = x
    list.y = y
    list.width = width
    list.height = height
    list.max_height = max_height
    list.toggle = false
    list.down = false
    list.scroll = 0

    local rx = layout.data.list.rx
    local ry = layout.data.list.ry

    list.mesh = gui.octagon(list.width, list.height, rx, ry)
    list.lines = gui.octagon(list.width, list.height, rx, ry, 'line')

    list.mesh_big = gui.octagon(list.width, list.max_height, rx, ry)
    list.lines_big = gui.octagon(list.width, list.max_height, rx, ry, 'line')

    return list
end

function gui.draw_label(label)
    if not label.text then
        return
    end

    love.graphics.setFont(label.font)
    love.graphics.setColor(label.color.r, label.color.g, label.color.b)
    love.graphics.printf(label.text, label.x, label.y, 
                         label.width, label.align)
end

function gui.draw_image(image)
    love.graphics.draw(image.img, image.quad, image.x, image.y, image.r)
end

function gui.draw_button(button)
    gui.drawoctagon(button.mesh, button.x, button.y, button.bcolor)
    if button.text then
        local _, wrap = button.font:getWrap(button.text, button.width)
        local height = button.font:getHeight() * #wrap
        local y = button.y + button.height/2 - height/2

        love.graphics.setFont(button.font)
        love.graphics.setColor(button.tcolor.r, button.tcolor.g, button.tcolor.b)
        love.graphics.printf(button.text, button.x, y, 
                             button.width, button.align)
    end
end

function gui.draw_slider(slider)
    local w, h = slider.width, slider.height

    love.graphics.setColor(0, 0, 0)
    love.graphics.setLineStyle('smooth')
    love.graphics.setLineWidth(layout.data.slider.line_width)
    love.graphics.translate(slider.x, slider.y)
    love.graphics.line(slider.left)
    love.graphics.line(slider.mid)
    love.graphics.line(slider.right)
    love.graphics.translate(-slider.x, -slider.y)

    local sx, sy = slider.x + ((slider.value-slider.min)/(slider.max-slider.min))
                   * slider.width,
                   slider.y + h/2

    love.graphics.draw(slider.slider, sx, sy)

    local v = math.floor(slider.value*100)/100
    love.graphics.printf(tostring(v), slider.x, slider.y+h/2, 
                         slider.width, 'center')
end

function gui.draw_checkbox(checkbox)
    -- inner
    gui.drawoctagon(checkbox.mesh, checkbox.x, checkbox.y, checkbox.bgcolor)

    -- outer
    gui.drawoctagon(checkbox.lines, checkbox.x, checkbox.y,
                    checkbox.fcolor, 'line', layout.data.checkbox.line_width)

    if checkbox.checked then
        love.graphics.setLineStyle('smooth')
        love.graphics.setLineWidth(2*layout.data.checkbox.line_width)
        love.graphics.line(checkbox.x, checkbox.y, 
                           checkbox.x+checkbox.width, checkbox.y+checkbox.height)
        love.graphics.line(checkbox.x+checkbox.width, checkbox.y,
                           checkbox.x, checkbox.y+checkbox.height)
    end
end

function gui.draw_container(container)
    if container.active then
        local function draw_stencil()
            love.graphics.rectangle('fill', container.x, container.y, 
                                    container.width, container.height)
        end

        -- inner
        gui.drawoctagon(container.mesh, container.x, container.y, container.bgcolor)

        -- outer
        gui.drawoctagon(container.lines, container.x, container.y,
                        container.fcolor, 'line', layout.data.container.line_width)

        love.graphics.stencil(draw_stencil)
        love.graphics.setStencilTest('greater', 0)
        gui.drawall(container.elements)
        love.graphics.setStencilTest()
    end
end

function gui.draw_container_scroll(container)
    if container.active then
        local function draw_stencil()
            love.graphics.rectangle('fill', container.x, container.y, 
                                    container.width, container.height)
        end

        -- inner
        gui.drawoctagon(container.mesh, container.x, container.y, container.bgcolor)

        -- outer
        gui.drawoctagon(container.lines, container.x, container.y,
                        container.fcolor, 'line', layout.data.container.line_width)

        love.graphics.stencil(draw_stencil)
        love.graphics.translate(container.scroll.x, container.scroll.y)
        love.graphics.setStencilTest('greater', 0)
        gui.drawall(container.elements)
        love.graphics.setStencilTest()

        love.graphics.translate(-container.scroll.x, -container.scroll.y)
    end
end

function gui.draw_textfield(textfield)
    local function draw_stencil()
        love.graphics.rectangle('fill', textfield.x, textfield.y, 
                                textfield.width, textfield.height)
    end

    -- inner
    gui.drawoctagon(textfield.mesh, textfield.x, textfield.y, textfield.bgcolor)

    -- outer
    gui.drawoctagon(textfield.lines, textfield.x, textfield.y,
                    textfield.fcolor, 'line', layout.data.textfield.line_width)

    love.graphics.stencil(draw_stencil)
    love.graphics.setStencilTest('greater', 0)
    local text = textfield.text
    if textfield.focus then
        text = text .. '|'
    end
    local offset = {}
    offset.x = textfield.font:getWidth('|') -- arbitrary offset to make
                                                     -- textfields more pleasant
                                                     -- to read
    -- vertical center
    local height = textfield.font:getHeight()
    offset.y = textfield.height/2 - height/2
    love.graphics.printf(text, textfield.x + offset.x, textfield.y + offset.y, 
                         textfield.width)
    love.graphics.setStencilTest()
end

function gui.draw_list(list)
    local offset = {}
    offset.x = list.font:getWidth('|') -- arbitrary offset to make
                                                     -- textfields more pleasant
                                                     -- to read
    -- vertical center
    local height = list.font:getHeight()
    offset.y = list.height/2 - height/2

    if list.toggle then
        local function draw_stencil()
            love.graphics.rectangle('fill', list.x, list.y, 
                                    list.width, list.max_height)
        end

        -- inner
        gui.drawoctagon(list.mesh_big, list.x, list.y, list.bgcolor)

        -- outer
        gui.drawoctagon(list.lines_big, list.x, list.y,
                        list.fcolor, 'line', layout.data.list.line_width)
    
        love.graphics.stencil(draw_stencil)
        love.graphics.setStencilTest('greater', 0)

        local scale = love.graphics.getWidth()/18

        local y = list.y+list.scroll
        for i, v in ipairs(list.elements) do
            if y > list.y + list.max_height then
                break
            end

            -- TODO: highlight selection
            love.graphics.printf(v.key, list.x+offset.x, y+offset.y, list.width)
            y = y + scale
        end
    
        love.graphics.setStencilTest()
    else
        local function draw_stencil()
            love.graphics.rectangle('fill', list.x, list.y, 
                                    list.width, list.height)
        end

        -- inner
        gui.drawoctagon(list.mesh, list.x, list.y, list.bgcolor)

        -- outer
        gui.drawoctagon(list.lines, list.x, list.y,
                        list.fcolor, 'line', layout.data.list.line_width)
    
        love.graphics.stencil(draw_stencil)
        love.graphics.setStencilTest('greater', 0)
        local text = ''
        if list.selection and list.selection.key then
            text = list.selection.key
        end
        love.graphics.printf(text, list.x+offset.x, list.y+offset.y, 
                             list.width)
    
        love.graphics.setStencilTest()
    end
end

function gui.draw(e)
    if e.t == nil then
        return
    elseif e.t == 'label' then
        gui.draw_label(e)
    elseif e.t == 'image' then
        gui.draw_image(e)
    elseif e.t == 'button' then
        gui.draw_button(e)
    elseif e.t == 'slider' then
        gui.draw_slider(e)
    elseif e.t == 'checkbox' then
        gui.draw_checkbox(e)
    elseif e.t == 'container' then
        gui.draw_container(e)
    elseif e.t == 'container.scroll' then
        gui.draw_container_scroll(e)
    elseif e.t == 'textfield' then
        gui.draw_textfield(e)
    elseif e.t == 'list' then
        gui.draw_list(e)
    end
end

function gui.drawall(elements)
    local later = {}
    for _, e in pairs(elements) do
        if e.t == 'container' or e.t == 'container.scroll' then
            table.insert(later, e)
        else
            gui.draw(e)
        end
    end

    for _, e in pairs(later) do
        gui.draw(e)
    end
end

function gui.interaction(e, mx, my, callback)
    if not callback or e.pause then
        return
    end

    if mx >= e.x and mx <= e.x+e.width
        and my >= e.y and my <= e.y+e.height then
        callback(e, mx, my)
    end
end

function gui.mousepressed(elements, mx, my)
    for _, e in pairs(elements) do
        if not e.pause then
            if e.x and e.y and e.width and e.height
                and mx >= e.x and mx <= e.x+e.width
                and my >= e.y and my <= e.y+e.height then

                if e.down ~= nil then
                    e.down = true
                end

                if e.t == 'checkbox' then
                    e.checked = not e.checked
                elseif e.t == 'textfield' and e.editable then
                    e.focus = true
                    love.keyboard.setTextInput(true)
                end

                if e.t == 'container' and e.active then
                    gui.mousepressed(e.elements, mx, my)
                elseif e.t == 'container.scroll' and e.active then
                    gui.mousepressed(e.elements, mx-e.scroll.x, my-e.scroll.y)
                end
            else
                if e.t == 'textfield' and e.editable then
                    e.focus = false
                end
            end

            if e.t == 'list' and e.toggle 
                and mx >= e.x and mx <= e.x+e.width
                and my >= e.y and my <= e.y+e.max_height then
                e.down = true
            end

            gui.interaction(e, mx, my, e.onClick)
        end
    end
end

function gui.mousereleased(elements, mx, my)
    for _, e in pairs(elements) do
        if not e.pause then
            if e.t == 'container' and e.active then
                gui.mousereleased(e.elements, mx, my)
            elseif e.t == 'container.scroll' and e.active then
                gui.mousereleased(e.elements, mx-e.scroll.x, my-e.scroll.y)
            elseif e.t == 'list' then
                local height = (e.toggle and e.max_height or e.height)
                if mx >= e.x and mx <= e.x+e.width
                    and my >= e.y and my <= e.y+height then

                    if e.toggle and not e.moved then
                        e.toggle = false
                        local scale = love.graphics.getWidth()/18
                        local sel = math.ceil((my-e.y-e.scroll)/scale)
                        if e.elements[sel] then
                            e.selection = e.elements[sel]
                        end
                        e.scroll = 0
                    elseif not e.toggle then
                        e.toggle = true
                    end

                end
                e.moved = false
            end

            if e.down == nil or e.down then
                gui.interaction(e, mx, my, e.onRelease)
            end

            if e.down ~= nil then
                e.down = false
            end
        end
    end
end

function gui.mousemoved(elements, mx, my, dx, dy)
    for _, e in pairs(elements) do
        if not e.pause then
            if e.t == 'container.scroll' and e.active and e.down then
                if not e.maxscroll or util.between(e.scroll.x+dx,
                    e.maxscroll.lo.x,
                    e.maxscroll.hi.x) then
                    e.scroll.x = e.scroll.x + dx
                end
                if not e.maxscroll or util.between(e.scroll.y+dy,
                    e.maxscroll.lo.y,
                    e.maxscroll.hi.y) then
                    e.scroll.y = e.scroll.y + dy
                end
            elseif e.t == 'list' and e.toggle and e.down then
                e.moved = true
                e.scroll = e.scroll + dy
            end


            if e.t == 'container' and e.active then
                gui.mousemoved(e.elements, mx, my, dx, dy)
            elseif e.t == 'container.scroll' and e.active then
                gui.mousemoved(e.elements, mx-e.scroll.x, my-e.scroll.y, dx, dy)
            end
        end
    end
end

function gui.wheelmoved(elements, dx, dy)
    for _, e in pairs(elements) do
        if not e.pause then
            if e.t == 'container.scroll' and e.active then
                local scale = love.graphics.getWidth()/18
                if not e.maxscroll or util.between(e.scroll.x+dx,
                    e.maxscroll.lo.x,
                    e.maxscroll.hi.x) then
                    e.scroll.x = e.scroll.x + dx*scale/2
                end
                if not e.maxscroll or util.between(e.scroll.y+dy,
                    e.maxscroll.lo.y,
                    e.maxscroll.hi.y) then
                    e.scroll.y = e.scroll.y + dy*scale/2
                end
            elseif e.t == 'list' then
                local scale = love.graphics.getWidth()/18
                e.scroll = e.scroll + dy*scale/2
            end
            if e.t == 'container' and e.active then
                gui.wheelmoved(e.elements, dx, dy)
            elseif e.t == 'container.scroll' and e.active then
                gui.wheelmoved(e.elements, dx, dy)
            end
        end
    end
end

function gui.textinput(elements, c)
    for _, e in pairs(elements) do
        if not e.pause then
            if e.t == 'textfield' and e.focus then
                e.text = e.text .. c
            elseif e.t == 'container' and e.active then
                gui.textinput(e.elements, c)
            elseif e.t == 'container.scroll' and e.active then
                gui.textinput(e.elements, c)
            end
        end
    end
end

function gui.keypressed(elements, key, scancode, isrepeat)
    for _, e in pairs(elements) do
        if not e.pause then
            if e.t == 'textfield' and e.focus then
                if scancode == 'escape' then
                    e.focus = false
                elseif scancode == 'backspace' then
                    e.text = string.sub(e.text, 1, #e.text-1)
                elseif scancode == 'return' then
                    e.text = e.text .. '\n'
                end
            elseif e.t == 'container' and e.active then
                gui.keypressed(e.elements, key, scancode, isrepeat)
            end
        end
    end

    -- on android the return key creates key 'escape' and scancode 'acback'
    if key == 'escape' or scancode == 'acback' then
        if love.keyboard.hasTextInput() then
            love.keyboard.setTextInput(false)
        elseif elements.escape then
            if elements.escape.onClick then
                elements.escape.onClick()
            elseif elements.escape.onRelease then
                elements.escape.onRelease()
            elseif elements.escape.onEscape then
                elements.escape.onEscape()
            end
        end
    end
end

function gui.update_slider(slider, mx, my)
    if slider.down 
        and mx >= slider.x and mx <= slider.x+slider.width
        and my >= slider.y and my <= slider.y+slider.height then
        slider.value = util.clamp(
            (mx-slider.x)/slider.width * (slider.max-slider.min) + slider.min,
            slider.min,
            slider.max)

        if slider.round == 'floor' then
            slider.value = math.floor(slider.value)
        elseif slider.round == 'ceil' then
            slider.value = math.ceil(slider.value)
        elseif slider.round == 'round' 
            or slider.round == true then
            slider.value = math.floor(slider.value+0.5)
        end
    end
end

function gui.update(elements)
    local mx, my = love.mouse.getPosition()
    for _, e in pairs(elements) do
        if not e.pause then
            if e.t == 'slider' then
                gui.update_slider(e, mx, my)
            elseif e.t == 'container' and e.active then
                gui.update(e.elements)
            end
            local callback = e.down and e.onHold or e.onHover
            gui.interaction(e, mx, my, e.onHover)
        end
    end
end

function gui.decode(obj, sx, sy, tx, ty)
    if type(obj) == 'table' and not obj.elements then
        return nil
    elseif type(obj) ~= 'table' then
        return obj
    end

    sx, sy = util.default(sx, 1), util.default(sy, 1)
    tx, ty = util.default(tx, 0), util.default(ty, 0)

    local out = {}
    out.elements = {}

    for k, e in pairs(obj.elements) do
        local x
        local y
        local width
        local height
        if e.x then x = math.floor(e.x*sx+tx+0.5) end
        if e.y then y = math.floor(e.y*sy+ty+0.5) end
        if e.width then width = math.floor(e.width*sx+0.5) end
        if e.height then height = math.floor(e.height*sy+0.5) end

        local font
        if e.font and type(e.font) == 'string' then
            font = layout[e.font]
        elseif e.font then
            font = e.font
        end
        if e.t == 'label' then
            out.elements[k] = 
                gui.label(x, y, width, height, 
                    e.text, e.align, font, e.color,
                    e.onHover)
        elseif e.t == 'image' then
            out.elements[k] = 
                gui.image(x, y, width, height, 
                    e.r, e.img, -- TODO: actually load the image
                    e.onHover)
        elseif e.t == 'button' then
            out.elements[k] = 
                gui.button(x, y, width, height, 
                    e.bcolor, e.text, e.align, font, e.tcolor,
                    e.onClick, e.onRelease, e.onHover, e.onHold)
        elseif e.t == 'slider' then
            out.elements[k] = 
                gui.slider(x, y, width, height, 
                    e.slider_width*sx,
                    e.min, e.max, e.def, e.round,
                    e.onClick, e.onRelease, e.onHover, e.onHold)
        elseif e.t == 'checkbox' then
            out.elements[k] = 
                gui.checkbox(x, y, width, height,
                    e.checked, e.bgcolor, e.fcolor,
                    e.onClick, e.onRelease, e.onHover, e.onHold)
        elseif e.t == 'container' then
            local elements = gui.decode(e, sx, sy, x, y).elements
            out.elements[k] = 
                gui.container(x, y, width, height,
                    elements, e.bgcolor, e.fcolor, e.active)
        elseif e.t == 'container.scroll' then
            local elements = gui.decode(e, sx, sy, x, y).elements
            local scroll = {lo = {}, hi = {}}
            if e.scroll.lo.x then scroll.lo.x = e.scroll.lo.x*sx end
            if e.scroll.lo.y then scroll.lo.y = e.scroll.lo.y*sy end
            if e.scroll.hi.x then scroll.hi.x = e.scroll.hi.x*sx end
            if e.scroll.hi.y then scroll.hi.y = e.scroll.hi.y*sy end
            out.elements[k] = 
                gui.container_scroll(x, y, width, height,
                    scroll, elements, e.bgcolor, e.fcolor, e.active)
        elseif e.t == 'textfield' then
            out.elements[k] = 
                gui.textfield(x, y, width, height, 
                    e.text, font, e.editable, e.bgcolor, e.fcolor,
                    e.onHover)
        elseif e.t == 'custom' then
            if e.x then e.x = x end
            if e.y then e.y = y end
            if e.width then e.width = width end
            if e.height then e.height = height end
            out.elements[k] = e
        end
    end

    for k, v in pairs(obj) do
        if type(v) == 'table' and v.t == 'custom' then
            if v.x then v.x = v.x*sx+tx end
            if v.y then v.y = v.y*sy+ty end
            if v.width then v.width = v.width*sx end
            if v.height then v.height = v.height*sy end
            out[k] = v
        elseif k ~= 'elements' then
            out[k] = gui.decode(v, sx, sy)
        end
    end

    return out
end

return gui
