
--[[ game.lua The actual gameplay.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local math = require('math')
local board = require('board')
local gui = require('gui')
local util = require('utils')
local json = require('dkjson')

local game = {}

function game.load(level, color, palette, t, cont)
    if cont then
        local path = 'save/savefile_' .. t .. '.prot'
        if not love.filesystem.exists(path) then
            return false
        end
    end
    
    state = {}
    state.name           = 'game'
    state.update         = game.update
    state.draw           = game.draw
    state.mousepressed   = game.mousepressed
    state.mousereleased  = game.mousereleased
    state.mousemoved     = game.mousemoved

    state.layout = {}
    state.layout.elements = layout.game.elements
    state.pause = false

    local mselect = {}
    mselect.x = 0
    mselect.y = 0
    mselect.dx = 0
    mselect.dy = 0
    mselect.rotate = nil
    mselect.down = false

    game.mselect = mselect

    game.t = util.default(t, 'r')

    for _, e in pairs(layout.game.elements) do
        if e.t == 'container' then
            e.active = false
        end
        if e.pause ~= nil then
            e.pause = false
        end
    end

    game.dopalette(palette, color)

    level = game.dolevel(level, color)
    game.dogoal()
    game.doshuffle(level, color)

    game.boardcopy = util.deepcopy(game.board)

    game.dosize()
    game.dotile()

    game.doscore()

    game.over = false

    local loadf
    if cont then
        loadf = game.loadf(game.t)
    end

    if not loadf and options.vibrate then
        love.system.vibrate(0.5)
    end
end

function game.dopalette(palette, color)
    if palette then
        game.palette = palette
    elseif color then
        game.palette = layout.palette.color
    else
        game.palette = layout.palette.normal
    end
end

function game.dolevel(level, color)
    if level then
        game.level = level
        if type(level) == 'table' then
            local t = level[1]
            local index = level[2]
            game.index = index
            if t == 'n' then
                level = levels.normal[index]
            elseif t == 'c' then
                level = levels.color[index]
            end
        end

        if level.palette then
            if type(level.palette) == 'string'
                and layout.palette[level.palette] then
                game.palette = layout.palette[level.palette]
            elseif type(level.palette) == 'table' then
                local i = 1
                while level.palette[tostring(i)]do
                    level.palette[i] = level.palette[tostring(i)]
                    level.palette[tostring(i)] = nil
                    i = i+1
                end
                game.palette = level.palette
            end
        end

        local new = {}
        new.width = level.width
        new.height = level.height
        for i = 1, level.height do
            new[i] = {}
            for j = 1, level.width do
                new[i][j] = level.board[i][j]
            end
        end

        game.board = new

        return level
    else
        game.level = 'random'
        local w
        local h
        local p

        if color then
            w = layout.main.play.color.elements.slider_1.value
            h = layout.main.play.color.elements.slider_2.value
            p = layout.main.play.color.elements.slider_4.value
        else
            w = layout.main.play.normal.elements.slider_1.value
            h = layout.main.play.normal.elements.slider_2.value
            p = layout.main.play.normal.elements.slider_4.value
        end

        game.board = board.new(w, h, p, #game.palette)
    end
end

function game.dogoal()
    local goal = {}
    goal.row = {}
    goal.col = {}
    for i = 1, game.board.height do
        goal.row[i] = board.check_row(game.board, i)
    end

    for i = 1, game.board.width do
        goal.col[i] = board.check_col(game.board, i)
    end

    game.goal = goal
end

function game.doshuffle(level, color)
    if level and level.shuffled then
        local shuffled = {}
        shuffled.width = level.width
        shuffled.height = level.height
        for i = 1, level.height do
            shuffled[i] = {}
            for j = 1, level.width do
                shuffled[i][j] = level.shuffled[i][j]
            end
        end

        game.board = shuffled

        game.shuffles = 0
    else
        local n
        if not level then
            if color then
                n = layout.main.play.color.elements.slider_3.value
            else
                n = layout.main.play.normal.elements.slider_3.value
            end
        else
            local w = game.board.width
            local h = game.board.height
            n = w*h-1 -- don't think about it,
                            -- it's mostly just a magic number
        end
        game.board = board.shuffle(game.board, n)

        game.shuffles = n -- used for scoring
    end
end

function game.dosize()
    local base_width = love.graphics.getWidth() - layout.game.board.x
    local base_height = base_width

    game.width = base_width
    game.height = base_height

    for _, g in ipairs(game.goal.row) do
        local text = ''
        for _, v in ipairs(g) do
            if options.colorblind then
                text = text .. ' ' .. string.char(v.c+0x60) .. '.' .. tostring(v.n)
            else
                text = text .. ' ' .. tostring(v.n)
            end
        end

        -- def_font is used for drawing side and top text
        local textw = def_font:getWidth(text)
        game.width = math.min(game.width, base_width - textw)
    end

    for _, g in ipairs(game.goal.col) do
        -- def_font is used for drawing side and top text
        local texth = def_font:getHeight() * #g
        game.height = math.min(game.height, base_height - texth)
    end

    game.width = math.min(game.width, game.height) - layout.game.board.sep
    game.height = game.width

    local base_x = 0
    local base_y = layout.game.board.y

    game.x = base_width - game.width - layout.game.board.sep
    game.y = base_y + base_height - game.height
end

function game.dotile()
    local tile = {}
    tile.width = math.floor(game.width / game.board.width)
    tile.height = math.floor(game.height / game.board.height)
    tile.sep = layout.game.board.sep
    tile.rx = layout.game.board.round
    tile.ry = layout.game.board.round

    tile.mesh = gui.octagon(tile.width-tile.sep, tile.height-tile.sep,
                            tile.rx, tile.ry)

    game.tile = tile
end

function game.doscore()
    -- values used for scoring
    -- 10pts per second
    -- 0.5 seconds is the base for 0x0
    -- time scales exponentially with log_2 of the total board size
    local exp = math.log(game.board.width*game.board.height)/math.log(2)
    game.score = 10*0.5*math.exp(exp)
    game.score_0 = game.score
    -- plus a fancy round
    game.score = math.floor(game.score/5)*5
    game.time = 0
    game.moves = 0
    game.colors = #game.palette > 1
end

function game.reset()
    game.board = util.deepcopy(game.boardcopy)
    game.time = 0
    game.score = game.score_0
end

function game.save(t, cache)
    if game.over then
        return
    end

    local save = {}
    save.board = game.board
    save.goal = game.goal
    save.time = game.time
    save.score = game.score
    save.moves = game.moves
    save.palette = game.palette
    save.index = game.index

    local save_json = json.encode(save)
    if not cache and not love.filesystem.exists('save') then
        love.filesystem.createDirectory('save')
    elseif cache and not love.filesystem.exists('cache') then
        love.filesystem.createDirectory('cache')
    end

    t = util.default(t, game.t)
    local path
    if not cache then
        path = 'save/savefile_' .. t .. '.prot'
    elseif cache then
        path = 'cache/savefile_' .. t .. '.prot'
    end
    love.filesystem.write(path, save_json)

    print('sucessfully saved game to ' .. path)

    if options.vibrate then
        love.system.vibrate(0.5)
    end
end

function game.loadf(t, cache)
    local path
    if not cache then
        path = 'save/savefile_' .. t .. '.prot'
    elseif cache then
        path = 'cache/savefile_' .. t .. '.prot'
    end

    if love.filesystem.exists(path) then
        local save_json = love.filesystem.read(path)
        love.filesystem.remove(path)
        local save = json.decode(save_json)

        for k, v in pairs(save.board) do
            local i = tonumber(k)
            if i then
                save.board[k] = nil
                save.board[i] = v
            end
        end

        game.board = save.board
        game.goal = save.goal
        game.time = save.time
        game.score = save.score
        game.moves = save.moves

        local i = 1
        while save.palette[tostring(i)] do
            save.palette[i] = save.palette[tostring(i)]
            save.palette[tostring(i)] = nil
            i = i+1
        end
        game.palette = save.palette
        game.index = save.index

        game.dosize()
        game.dotile()

        if options.vibrate then
            love.system.vibrate(0.5)
        end

        return true
    end
end

function game.savehi(score)
    if not love.filesystem.exists('hi') then
        love.filesystem.createDirectory('hi')
    end

    if score > game.loadhi() then
        -- TODO: new highscore notification
        local path
        if game.colors then
            path = 'hi/color_' .. game.t .. '.prot'
        else
            path = 'hi/normal_' .. game.t .. '.prot'
        end

        local time = os.time()
        love.filesystem.write(path, tostring(time)..' '..tostring(score))

        game.highscore = score

        return true
    end
end

function game.loadhi(path)
    if not path then
        if game.colors then
            path = 'hi/color_' .. game.t .. '.prot'
        else
            path = 'hi/normal_' .. game.t .. '.prot'
        end
    end

    local val, count = love.filesystem.read(path)
    if val and count > 0 then
        local res = util.match(val, '%d+')
        local time = tonumber(res[1])
        local score = tonumber(res[2])
        game.highscore = score

        return score, time
    else
        game.highscore = -1e309 -- negative infinity

        return -1e309, 0
    end
end

function game.check_win()
    for row = 1, game.board.height do
        local delta = board.check_row(game.board, row)
        if not util.cmp_table(game.goal.row[row], delta) then
            return false
        end
    end

    for col = 1, game.board.width do
        local delta = board.check_col(game.board, col)
        if not util.cmp_table(game.goal.col[col], delta) then
            return false
        end
    end

    return true
end

function game.mousereleased(x, y, button)
    if button == 1 then
        if game.mselect.rotate == 'x' then
            local rot = math.floor(game.mselect.dx / game.tile.width + 0.5)
            board.rotate_x(game.board, game.mselect.y, rot)

            if rot ~= 0 then
                game.moves = game.moves+1
            end
        end

        if game.mselect.rotate == 'y' then
            local rot = math.floor(game.mselect.dy / game.tile.height + 0.5)
            board.rotate_y(game.board, game.mselect.x, rot)

            if rot ~= 0 then
                game.moves = game.moves+1
            end
        end

        game.mselect.x = 0
        game.mselect.y = 0
        game.mselect.dx = 0
        game.mselect.dy = 0
        game.mselect.rotate = nil
        game.mselect.down = false
    end
end

function game.mousepressed(x, y, button)
    if button == 1 then
        game.mselect.x = math.floor((x-game.x) / game.tile.width) + 1
        game.mselect.y = math.floor((y-game.y) / game.tile.height) + 1

        if util.between(game.mselect.x, 1, game.board.width)
            and util.between(game.mselect.y, 1, game.board.height) then
            game.mselect.down = true
        else
            game.mselect.x = 0
            game.mselect.y = 0
        end
    end
end

function game.mousemoved(x, y, dx, dy)
    if game.mselect.down then
        game.mselect.dx = game.mselect.dx + dx
        game.mselect.dy = game.mselect.dy + dy

        if math.abs(game.mselect.dx) > math.abs(game.mselect.dy) then
            game.mselect.rotate = 'x'
        else
            game.mselect.rotate = 'y'
        end
    end
end

function game.update(dt)
    if not game.over then
        game.time = game.time + dt
        game.score = math.max(game.score - 10*dt, 0)
    
        local win = game.check_win()
    
        if win then 
            local w = game.board.width
            local h = game.board.height
    
            local score
            local time = game.score
            local size = w*h
            local moves = game.moves 
            local shuffles = game.shuffles 
            local move_score = (shuffles-moves)/shuffles
            if shuffles == 0 then
                move_score = 0
            elseif move_score >= 0 then
                -- Don't bother understanding this.
                -- You might as well accept this as a magic number/formula.
                --  (e^(x-1) - 1/e) / (1 - 1/e)
                --  e_m_one = 1/e
                -- This basically means that with a perfect (impossible) game,
                --  the bonus for moves is equal to the bonus for time.
                -- But for a bad game, this bonus is going to be very little.
                -- And if the number of moves exceeds the number of shuffles,
                --  the player gets penalized.
                local e_m_one = 0.36787944117144
                move_score = game.score_0*(math.exp(move_score-1)-e_m_one)/(1-e_m_one)
            else
                move_score = -game.score_0*move_score*move_score
            end
    
            score = math.floor(time + move_score)
            game.score = score
    
            game.over = true
            state.pause = true
            state.layout.elements = layout.game.over.elements

            local newhigh = game.savehi(score)
            game.newhigh = newhigh

            if options.vibrate then
                if newhigh then
                    love.system.vibrate(1.5)
                else
                    love.system.vibrate(0.5)
                end
            end

            -- new game board size and position
            game.width = love.graphics.getWidth() * 3/4
            game.height = game.width
            game.x = (love.graphics.getWidth() * 1/4) / 2
            game.y = love.graphics.getHeight() * 5/16

            game.dotile()

            -- save completed levels
            if game.index then
                local complete
                if love.filesystem.exists('lvl-complete.prot') then
                    local complete_json = love.filesystem.read('lvl-complete.prot')
                    complete = json.decode(complete_json)
                else
                    complete = {normal = {}, color = {}}
                end

                if game.colors then
                    table.insert(complete.color, game.index)

                    -- set color to random pale blueish color (from gray)
                    local color = {}
                    color.r = math.random(0, 96)
                    color.g = math.random(32, 128)
                    color.b = math.random(96, 192)
                    layout.main.clevels.elements.levels.elements[game.index].bcolor =
                        color
                else
                    table.insert(complete.normal, game.index)

                    -- set color to black (from gray)
                    local color = {r = 0, g = 0, b = 0}
                    layout.main.nlevels.elements.levels.elements[game.index].bcolor =
                        color
                end

                local complete_json = json.encode(complete)
                love.filesystem.write('lvl-complete.prot', complete_json)
            end
        end
    end
end

function game.text_row(x, y, t)
    love.graphics.setFont(def_font)
    local offset = 0
    for i = #t, 1, -1 do
        if #game.palette > 1 and options.colorblind then
            local fn_width = love.graphics.getFont():getWidth(' ' ..
                                string.char(0x60+t[i].c) .. '.' ..
                                tostring(t[i].n))
            love.graphics.setColor(0, 0, 0)
            love.graphics.printf(string.char(0x60+t[i].c) .. '.' ..
                                 tostring(t[i].n),
                                 x, y, game.x-offset, 'right')
            offset = offset + fn_width
        else
            local fn_width = love.graphics.getFont():getWidth(' ' .. tostring(t[i].n))
            local color = game.palette[t[i].c]
            love.graphics.setColor(color.r, color.g, color.b)
            love.graphics.printf(tostring(t[i].n), x, y, 
                                 game.x-offset, 'right')
            offset = offset + fn_width
        end
    end
end

function game.text_col(x, y, t)
    love.graphics.setFont(def_font)
    local fn_height = love.graphics.getFont():getHeight()
    for i = #t, 1, -1 do
        if #game.palette > 1 and options.colorblind then
            love.graphics.setColor(0, 0, 0)
            love.graphics.printf(string.char(0x60+t[i].c) .. '.' ..
                                 tostring(t[i].n),
                                 x, y-fn_height*(#t-i+1), game.tile.width, 'center')
        else
            local color = game.palette[t[i].c]
            love.graphics.setColor(color.r, color.g, color.b)
            love.graphics.printf(tostring(t[i].n), x, y-fn_height*(#t-i+1), 
                                 game.tile.width, 'center')
        end
    end
end

function game.draw_label(text, x, y, width, color, align, font, r)
    color = util.default(color, {r = 0, g = 0, b = 0})
    font = util.default(font, def_font)
    love.graphics.setFont(font)
    love.graphics.setColor(color.r, color.g, color.b)
    love.graphics.printf(text, x, y, width, align, r)
end

function game.draw_stencil()
    love.graphics.rectangle('fill', game.x, game.y, 
                            game.width, game.height)
end

function game.draw()
    love.graphics.setFont(def_font)
    love.graphics.setColor(0, 0, 0)


    -- game field
    love.graphics.stencil(game.draw_stencil, 'replace', 1)
    love.graphics.setStencilTest('greater', 0)

    love.graphics.setColor(layout.game.board.color.r,
                           layout.game.board.color.g,
                           layout.game.board.color.b)
    love.graphics.rectangle('fill', game.x, game.y, 
                            game.width, game.height)

    love.graphics.push()

    love.graphics.translate(game.x, game.y)

    for i = 1, game.board.height do
        for j = 1, game.board.width do
            if game.board[i][j] > 0 then
                local x = (j-1)*game.tile.width  + game.tile.sep/2
                local y = (i-1)*game.tile.height + game.tile.sep/2
                local w = game.tile.width-game.tile.sep
                local h = game.tile.height-game.tile.sep

                local c = game.board[i][j]

                if #game.palette > 1 and options.colorblind then
                    love.graphics.setColor(0, 0, 0)
                else
                    local color = game.palette[c]
                    love.graphics.setColor(color.r, color.g, color.b)
                end

                if game.mselect.rotate == 'x' 
                    and game.mselect.y == i then
                    x = x + game.mselect.dx
                elseif game.mselect.rotate == 'y'
                    and game.mselect.x == j then
                    y = y + game.mselect.dy
                end

                local draw = (x > -w) and (x < game.width)
                         and (y > -h) and (y < game.height)

                if draw then
                    love.graphics.draw(game.tile.mesh, x, y)
                    if #game.palette > 1 and options.colorblind then
                        local fn_height = love.graphics.getFont():getHeight()
                        love.graphics.setColor(255, 255, 255)
                        love.graphics.printf(string.char(0x60+c),
                                             x, y+h/2-fn_height/2,
                                             w, 'center')
                    end
                end

                -- very important booleans
                -- without them the while loop loops infinitely when trying to wrap
                local lx = x < 0
                local ly = y < 0
                local gx = x+w > game.width
                local gy = y+h > game.height

                local wrap = true
                while wrap do
                    wrap = false
                    if lx and x < 0 then
                        x = x + game.width
                        wrap = true
                    elseif gx and x+w > game.width then
                        x = x - game.width
                        wrap = true
                    end

                    if ly and y < 0 then
                        y = y + game.height
                        wrap = true
                    elseif gy and y+h > game.height then
                        y = y - game.height
                        wrap = true
                    end

                    if wrap then
                        love.graphics.draw(game.tile.mesh, x, y)
                        if #game.palette > 1 and options.colorblind then
                            local fn_height = love.graphics.getFont():getHeight()
                            love.graphics.setColor(255, 255, 255)
                            love.graphics.printf(string.char(0x60+c),
                                                 x, y+h/2-fn_height/2,
                                                 w, 'center')
                        end
                    end
                end
            end
        end
    end

    love.graphics.setStencilTest()
    love.graphics.pop()

    if game.over then
        local width = love.graphics.getWidth()
        local scale = width/18
        if game.newhigh then
            -- time
            local t = game.time
            local s = math.floor(t%60)
            local m = math.floor(t/60)
            local time = string.format('Time: %02d:%02d', m, s)
            game.draw_label(time, 0, math.floor(7*scale), width, nil,
                            'center', layout.font_big)

            -- highscore
            local highscore = string.format('New highscore\n%d!', game.highscore)
            game.draw_label(highscore, 0, math.floor(6*scale), width, nil,
                            'center', layout.font_big, -math.rad(20))
        else
            -- score
            local score = string.format('Score: %d', game.score)
            game.draw_label(score, 0, math.floor(2*scale), width, nil,
                            'center', layout.font_large)

            -- time
            local t = game.time
            local s = math.floor(t%60)
            local m = math.floor(t/60)
            local time = string.format('Time: %02d:%02d', m, s)
            game.draw_label(time, 0, math.floor(5*scale), width, nil,
                            'center', layout.font_big)

            -- highscore
            local highscore = string.format('Highscore: %d', game.highscore)
            game.draw_label(highscore, 0, math.floor(7*scale), width, nil,
                            'center', layout.font_big)
        end
    else
        love.graphics.setColor(0, 0, 0)
        -- numbers
        for i = 1, game.board.width do
            game.text_col(game.x+game.tile.width*(i-1),
                          game.y,
                          game.goal.col[i])
        end

        for i = 1, game.board.height do
            local fn_height = love.graphics.getFont():getHeight()
            local offset = game.tile.height/2-fn_height/2
            game.text_row(0,
                          game.y+game.tile.height*(i-1)+offset,
                          game.goal.row[i])
        end

        -- time
        local t = game.time
        local s = math.floor(t%60)
        local m = math.floor(t/60)
        local time = string.format('Time: %02d:%02d', m, s)
        game.draw_label(time, game.x,
                        game.y+game.height,
                        game.width/2)

        -- score
        local score = string.format('Score: %d', game.score)
        game.draw_label(score, game.x+game.width/2,
                        game.y+game.height,
                        game.width/2)
    end
end

return game
