
--[[ level.lua Custom and built-in level loading.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local util = require('utils')
local json = require('dkjson')
local table = require('table')
local gui = require('gui')
local game = require('game')
local loadgame = require('loadgame')

local level = {}

function level.load(t)
    t = util.default(t, 'n')
    local l = {}

    local i = 1
    while love.filesystem.exists(string.format('lvl/%s%02d.json', t, i)) do
        local f_json = love.filesystem.read(
                                string.format('lvl/%s%02d.json', t, i))
        local lvl = json.decode(f_json)
        if lvl.shuffled then
            lvl.shuffled.width = lvl.width
            lvl.shuffled.height = lvl.height
        end
        table.insert(l, lvl)
        i = i+1
    end

    return l
end

function level.guigen(levels, sx, sy, t)
    sx = util.default(sx, 1)
    sy = util.default(sy, 1)
    local g = {}
    g.elements = {}
    g.elements.levels = gui.container_scroll(0*sx, 2*sy, 18*sx, 20*sy,
                                             {lo = {x = 0, y = nil},
                                              hi = {x = 0, y = 0}}, {},
                                             {r = 255, g = 255, b = 255},
                                             {r = 255, g = 255, b = 255}, true)

    local x, y = 1, 4
    local w, h = 4, 4
    local gray = {r = 96, b = 96, g = 96}
    local white = {r = 255, b = 255, g = 255}
    local black = {r = 0, b = 0, g = 0}

    for i, l in ipairs(levels) do
        local button = gui.button(x*sx, y*sy, w*sx, h*sy, gray, 
                                  tostring(i) .. '\n' .. l.name, 'center',
                                  nil, white, nil, function()
                                     loadgame.load(i, l.name, {t, i}, nil, nil, 'l')
                                  end)

        table.insert(g.elements.levels.elements, button)
        
        x = x+8
        if x == 17 then
            x = 5
            y = y + 4
        elseif x == 21 then
            x = 1
            y = y + 4
        end
    end

    if love.filesystem.exists('lvl-complete.prot') then
        local complete_json = love.filesystem.read('lvl-complete.prot')
        local complete = json.decode(complete_json)

        complete = (t == 'n' and complete.normal) or (t == 'c' and complete.color)
        for _, i in ipairs(complete) do
            local color
            if t == 'n' then
                color = black
            else -- random bluish color
                color = {}
                color.r = math.random(0, 96)
                color.g = math.random(32, 128)
                color.b = math.random(96, 192)
            end
            g.elements.levels.elements[i].bcolor = color
        end
    end

    g.elements.escape = gui.button(1*sx, 25*sy, 4*sx, 4*sy,
                        black, 'Back', 'center', nil, nil, nil,
                        function()
                           state.layout.elements = layout.main.elements
                        end)
    table.insert(g.elements, gui.button(9*sx, 25*sy, 8*sx, 4*sy,
                             black, 'Continue', 'center', nil, nil, nil,
                             function()
                                game.load(nil, t == 'c', nil, 'l', true)
                             end))

    return g
end

function level.load_custom(sx, sy, l_list)
    if love.filesystem.exists('custom-levels.prot') then
        local l_json, count = love.filesystem.read('custom-levels.prot')
        if count > 0 then
            l_list = json.decode(l_json).list
        end
    else
        l_list = {}
    end

    local l = {}
    local g = {}
    g.elements = {}
    g.elements.list = gui.list(1*sx, 10*sy, 10*sx, 1*sy, 6*sy, {},
                                 {r = 255, g = 255, b = 255})

    local elements = g.elements.list.elements

    for i, v in ipairs(l_list) do
        local lvl_json, count = love.filesystem.read(v)
        if lvl_json and count > 0 then
            local lvl = json.decode(lvl_json)

            table.insert(l, lvl)
            table.insert(elements, {key = lvl.name, value = lvl})
        end
    end

    g.elements.levels = gui.container_scroll(0*sx, 2*sy, 18*sx, 20*sy,
                                             {lo = {x = 0, y = nil},
                                              hi = {x = 0, y = 0}}, {},
                                             {r = 255, g = 255, b = 255},
                                             {r = 255, g = 255, b = 255}, true)

    local x, y = 1, 4
    local w, h = 4, 4
    local color = {r = 0, b = 0, g = 0}

    for i, v in ipairs(l_list) do
        local lvl_json, count = love.filesystem.read(v)
        if lvl_json and count > 0 then
            local lvl = json.decode(lvl_json)
            local button = gui.button(x*sx, y*sy, w*sx, h*sy, color, 
                                      lvl.name, 'center',
                                      nil, nil, nil, function()
                                         loadgame.load(lvl.name, nil,
                                                       lvl, nil, nil, 'c')
                                      end)

            table.insert(g.elements.levels.elements, button)
            
            x = x+8
            if x == 17 then
                x = 5
                y = y + 4
            elseif x == 21 then
                x = 1
                y = y + 4
            end
        end
    end

    g.elements.escape = gui.button(1*sx, 25*sy, 4*sx, 4*sy,
                        color, 'Back', 'center', nil, nil, nil,
                        function()
                           state.layout.elements = layout.main.elements
                        end)
    table.insert(g.elements, gui.button(9*sx, 25*sy, 8*sx, 4*sy,
                             color, 'Continue', 'center', nil, nil, nil,
                             function()
                                game.load(nil, nil, nil, 'c', true)
                             end))

    return g
end

function level.update_customlist(l)
    if love.filesystem.exists('custom-levels.prot') then
        local l_json, count = love.filesystem.read('custom-levels.prot')
        if count == 0 then
            local l_json = json.encode({list = l})
            love.filesystem.write('custom-levels.prot', l_json)
            return l
        else
            local l_current = json.decode(l_json)
            local l_new = util.concat(l_current.list, l)

            -- remove duplicates
            local hash = {}
            local l_nodup = {list = {}}
            for _, v in ipairs(l_new) do
                if not hash[v] then
                    table.insert(l_nodup.list, v)
                    hash[v] = true
                end
            end

            local l_json = json.encode(l_nodup)
            love.filesystem.write('custom-levels.prot', l_json)
            return l_nodup.list
        end
    else
        local l_json = json.encode({list = l})
        love.filesystem.write('custom-levels.prot', l_json)
        return l
    end
end

return level

