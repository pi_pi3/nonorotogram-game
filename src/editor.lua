
--[[ editor.lua Built-in level editor.
    Copyright (c) 2017 Szymon "pi_pi3" Walter
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
    
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
    
    3. This notice may not be removed or altered from any source
    distribution.
]]

local gui = require('gui')
local math = require('math')
local board = require('board')
local util = require('utils')
local json = require('dkjson')
local level = require('level')

local editor = {}

function editor.load(color, palette)
    state = {}
    state.name           = 'editor'
    state.update         = editor.update
    state.draw           = editor.draw
    state.mousepressed   = editor.mousepressed
    state.mousereleased  = editor.mousereleased
    state.mousemoved     = editor.mousemoved

    state.layout = {}
    state.layout.elements = layout.editor.elements
    state.pause = false

    for _, e in pairs(layout.editor.elements) do
        if e.t == 'container' then
            e.active = false
        end
        if e.pause ~= nil then
            e.pause = false
        end
    end

    editor.colorid = 1

    editor.grid = layout.main.editor.elements.checkbox_1.checked
    editor.name = layout.main.editor.elements.textfield_1.text

    local w = layout.main.editor.elements.slider_1.value
    local h = layout.main.editor.elements.slider_2.value

    editor.board = board.new(w, h, 0, 1)
    editor.shuffled = nil

    editor.edited = editor.board

    editor.dodelta()

    editor.dosize()
    editor.dotile()

    editor.dopalette(palette, color)

    if options.vibrate then
        love.system.vibrate(0.5)
    end
end

function editor.dodelta()
    local delta = {}
    delta.row = {}
    delta.col = {}
    for i = 1, editor.board.height do
        delta.row[i] = board.check_row(editor.board, i)
    end

    for i = 1, editor.board.width do
        delta.col[i] = board.check_col(editor.board, i)
    end

    editor.delta = delta
end

function editor.dosize()
    local base_width = love.graphics.getWidth() - layout.editor.board.x
    local base_height = base_width

    editor.width = base_width
    editor.height = base_height

    for _, g in ipairs(editor.delta.row) do
        local text = ''
        for _, v in ipairs(g) do
            if options.colorblind then
                text = text .. ' ' .. string.char(v.c+0x60) .. '.' .. tostring(v.n)
            else
                text = text .. ' ' .. tostring(v.n)
            end
        end

        -- def_font is used for drawing side and top text
        local textw = def_font:getWidth(text)
        editor.width = math.min(editor.width, base_width - textw)
    end

    for _, g in ipairs(editor.delta.col) do
        -- def_font is used for drawing side and top text
        local texth = def_font:getHeight() * #g
        editor.height = math.min(editor.height, base_height - texth)
    end

    editor.width = math.min(editor.width, editor.height) - layout.editor.board.sep
    editor.height = editor.width

    local base_x = 0
    local base_y = layout.editor.board.y

    editor.x = base_width - editor.width - layout.editor.board.sep
    editor.y = base_y + base_height - editor.height
end

function editor.dotile()
    local tile = {}
    tile.width = math.floor(editor.width / editor.board.width)
    tile.height = math.floor(editor.height / editor.board.height)
    tile.sep = layout.editor.board.sep
    tile.rx = layout.editor.board.round
    tile.ry = layout.editor.board.round

    tile.mesh = gui.octagon(tile.width-tile.sep, tile.height-tile.sep,
                            tile.rx, tile.ry)

    editor.tile = tile
end

function editor.dopalette(palette, color)
    local i = 1
    while palette and palette[tostring(i)] do
        palette[i] = palette[tostring(i)]
        palette[tostring(i)] = nil
        i = i+1
    end

    editor.color = color
    if palette then
        palette = palette
    elseif not color then
        palette = layout.palette.normal
    else
        palette = layout.palette.color
    end

    if type(palette) == 'string' and layout.palette[palette] then
        palette = layout.palette[palette]
    end

    editor.palette = palette

    if editor.palette ~= layout.palette.normal then
        local scale = love.graphics.getWidth()/18

        layout.editor.elements.palettec = gui.container_scroll(
                                          4.5*scale, 0*scale, 14*scale, 3*scale,
                                          {lo = {x = nil, y = 0},
                                           hi = {x = 0, y = 0}}, {},
                                          {r = 255, g = 255, b = 255},
                                          {r = 255, g = 255, b = 255}, true)
        local palettec = layout.editor.elements.palettec


        local x = 5
        local y = 0.5
        local w = 2
        local h = 2

        for i, c in ipairs(editor.palette) do
            local text = ''
            if options.colorblind then
                text = string.char(0x60+i)
                c = {r = 0, g = 0, b = 0}
            end
            table.insert(palettec.elements, gui.button(
                                            (x+(i-1)*w)*scale, y*scale, 
                                            w*scale, h*scale,
                                            c, text, 'center', nil, nil, nil, 
                                            function()
                                               editor.colorid = i
                                            end))
        end
    else
        layout.editor.elements.palettec = nil
    end
end

function editor.clear()
    editor.board = board.new(editor.board.width, editor.board.height, 0, 1)
    editor.shuffled = nil
    editor.edited = editor.board
    editor.dodelta()
    editor.dosize()
    editor.dotile()
end

function editor.mousepressed(x, y, button)
    if button == 1 then
        local mx = math.floor((x-editor.x) / editor.tile.width) + 1
        local my = math.floor((y-editor.y) / editor.tile.height) + 1

        if util.between(mx, 1, editor.edited.width)
            and util.between(my, 1, editor.edited.height) then
            if editor.edited[my][mx] ~= editor.colorid then
                editor.edited[my][mx] = editor.colorid
            else
                editor.edited[my][mx] = 0
            end

            if editor.edited == editor.board then
                -- recalculate delta
                local delta = {}
                delta.row = {}
                delta.col = {}
                for i = 1, editor.edited.height do
                    delta.row[i] = board.check_row(editor.edited, i)
                end

                for i = 1, editor.edited.width do
                    delta.col[i] = board.check_col(editor.edited, i)
                end

                editor.delta = delta
            end
        end

        editor.dosize()
        editor.dotile()
    end
end

function editor.save(name, cache)
    name = util.default(name, editor.name)
    if not cache and not util.endswith(name, '.json') then
        name = name .. '.json'
    end

    if not cache and util.startswith(name, 'cache/') then
        print('error: cannot save custom level to cache')
    end

    local tilen = {}
    local shufn = {}

    local lvl = {}
    lvl.name = editor.name
    lvl.width = editor.board.width
    lvl.height = editor.board.height
    lvl.board = {}
    for i = 1, lvl.height do
        lvl.board[i] = {}
        for j = 1, lvl.width do
            local c = editor.board[i][j]
            lvl.board[i][j] = c
            if c ~= 0 then
                if not tilen[c] then
                    tilen[c] = 0
                end
                tilen[c] = tilen[c]+1
            end
        end
    end
    if editor.shuffled then
        lvl.shuffled = {}
        local isnil = true
        for i = 1, lvl.height do
            lvl.shuffled[i] = {}
            for j = 1, lvl.width do
                local c = editor.shuffled[i][j]
                lvl.shuffled[i][j] = c
                if c ~= 0 then
                    isnil = false
                    if not shufn[c] then
                        shufn[c] = 0
                    end
                    shufn[c] = shufn[c]+1
                end
            end
        end
        if isnil then 
            lvl.shuffled = nil
        elseif not cache and not util.cmp_table(tilen, shufn) then
            print('error: '..
                  'number of tiles on board and pre-shuffled board must be equal')
            return
        elseif not cache and util.cmp_table(lvl.board, lvl.shuffled) then
            print('error: board and pre-shuffled board must not be equal')
            return
        end
    end

    if editor.palette == layout.palette.normal then
        lvl.palette = 'normal'
    elseif editor.palette == layout.palette.color then
        lvl.palette = 'color'
    else
        lvl.palette = editor.palette
    end

    local lvl_json = json.encode(lvl)

    local success = love.filesystem.write(name, lvl_json)
    if not success then
        print('error: couldn\'t save to file ' .. name) 
        return
    end

    if not cache then
        local l_list = level.update_customlist({name})
        
        local scale = love.graphics.getWidth()/18
        layout.main.custom = level.load_custom(scale, scale, l_list)
    end

    if options.vibrate then
        love.system.vibrate(0.5)
    end
end

function editor.loadf(name, cache)
    name = util.default(name, editor.name)
    if not cache and not util.endswith(name, '.json') then
        name = name .. '.json'
    end

    local lvl_json, size = love.filesystem.read(name)
    if size == 0 then
        print('error: couldn\'t load file ' .. name) 
        return
    end

    local lvl = json.decode(lvl_json)

    local new = {}
    new.width = lvl.width
    new.height = lvl.height
    for i = 1, lvl.height do
        new[i] = {}
        for j = 1, lvl.width do
            new[i][j] = lvl.board[i][j]
        end
    end

    editor.name = lvl.name
    editor.board = new
    editor.edited = new

    if lvl.shuffled then
        local shuffled = {}
        shuffled.width = lvl.width
        shuffled.height = lvl.height
        for i = 1, lvl.height do
            shuffled[i] = {}
            for j = 1, lvl.width do
                shuffled[i][j] = lvl.shuffled[i][j]
            end
        end
        editor.shuffled = shuffled
    end

    editor.dopalette(lvl.palette)

    editor.dodelta()

    editor.dosize()
    editor.dotile()

    if options.vibrate then
        love.system.vibrate(0.5)
    end
end

function editor.text_row(x, y, t)
    love.graphics.setFont(def_font)
    local offset = 0
    for i = #t, 1, -1 do
        if #editor.palette > 1 and options.colorblind then
            local fn_width = love.graphics.getFont():getWidth(' ' ..
                                string.char(0x60+t[i].c) .. '.' ..
                                tostring(t[i].n))
            love.graphics.setColor(0, 0, 0)
            love.graphics.printf(string.char(0x60+t[i].c) .. '.' ..
                                 tostring(t[i].n),
                                 x, y, editor.x-offset, 'right')
            offset = offset + fn_width
        else
            local fn_width = love.graphics.getFont():getWidth(' ' .. tostring(t[i].n))
            local color = editor.palette[t[i].c]
            love.graphics.setColor(color.r, color.g, color.b)
            love.graphics.printf(tostring(t[i].n), x, y, 
                                 editor.x-offset, 'right')
            offset = offset + fn_width
        end
    end
end

function editor.text_col(x, y, t)
    love.graphics.setFont(def_font)
    local fn_height = love.graphics.getFont():getHeight()
    for i = #t, 1, -1 do
        if #editor.palette > 1 and options.colorblind then
            love.graphics.setColor(0, 0, 0)
            love.graphics.printf(string.char(0x60+t[i].c) .. '.' ..
                                 tostring(t[i].n),
                                 x, y-fn_height*(#t-i+1), editor.tile.width, 'center')
        else
            local color = editor.palette[t[i].c]
            love.graphics.setColor(color.r, color.g, color.b)
            love.graphics.printf(tostring(t[i].n), x, y-fn_height*(#t-i+1), 
                                 editor.tile.width, 'center')
        end
    end
end

function editor.draw()
    love.graphics.setFont(def_font)
    love.graphics.setColor(0, 0, 0)

    for i = 1, editor.edited.width do
        editor.text_col(editor.x+editor.tile.width*(i-1),
                        editor.y,
                        editor.delta.col[i])
    end

    for i = 1, editor.edited.height do
        local fn_height = love.graphics.getFont():getHeight()
        local offset = editor.tile.height/2-fn_height/2
        editor.text_row(0,
                      editor.y+editor.tile.height*(i-1)+offset,
                      editor.delta.row[i])
    end

    -- editor field
    love.graphics.setColor(layout.editor.board.color.r,
                           layout.editor.board.color.g,
                           layout.editor.board.color.b)
    love.graphics.rectangle('fill', editor.x, editor.y, 
                            editor.width, editor.height)
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle('line', editor.x, editor.y, 
                           editor.width, editor.height)

    love.graphics.push()

    love.graphics.translate(editor.x, editor.y)

    if editor.grid then
        love.graphics.setColor(0, 0, 0)
        love.graphics.setLineWidth(1)
        for i = 1, editor.edited.width-1 do
            love.graphics.line(editor.tile.width*i, 0,
                               editor.tile.width*i, editor.height)
        end
        for j = 1, editor.edited.height-1 do
            love.graphics.line(0,            editor.tile.height*j, 
                               editor.width, editor.tile.height*j)
        end
    end

    for i = 1, editor.edited.height do
        for j = 1, editor.edited.width do
            if editor.edited[i][j] > 0 then
                local x = (j-1)*editor.tile.width  + editor.tile.sep/2
                local y = (i-1)*editor.tile.height + editor.tile.sep/2

                local c = editor.edited[i][j]
                if #editor.palette > 1 and options.colorblind then
                    love.graphics.setColor(0, 0, 0)
                else
                    local color = editor.palette[c]
                    love.graphics.setColor(color.r, color.g, color.b)
                end

                love.graphics.draw(editor.tile.mesh, x, y)
                if #editor.palette > 1 and options.colorblind then
                    local fn_height = love.graphics.getFont():getHeight()
                    love.graphics.setColor(255, 255, 255)
                    love.graphics.printf(string.char(0x60+c),
                                         x, y+editor.tile.height/2-fn_height/2,
                                         editor.tile.width, 'center')
                end
            end
        end
    end

    love.graphics.pop()
end

return editor
