
# Factsheet
## Developer:
Drifting in Yellow / Szymon "pi_pi3" Walter

Based in Berlin, Germany

## Release date:
Beta release: March 12th, 2017

Full release: April 2nd, 2017

## Platforms:
Windows, OS/X, Linux, Android

(web in plans)

## Website/blog:
https://driftinginyellow.wordpress.com

## Regular price:
0.00€

(Donations above 0.00€ accepted.)

# Description
NoNo! RotoGram! is a puzzle game about sliding. Think nonograms + Rubik's cube + 2D. It
combines the rotation of a Rubik's cube and the scheme of a nonogram. The game can be
played in one of two modes: normal and color. One only features black tiles, the other 
uses multiple colors for different tiles. The game has built-in levels for the player
to play and also features an option for endless randomized challenges. For the 
particularly creative players there's also the built-in level editor with a palette
editor. 

# How to play
1. Download from: [itch.io](https://pi-pi3.itch.io/nono-rotogram)
2. Follow the install instructions on the page above.
3. I recommend you to start playing from built-in levels on normal mode.
4. The numbers on top and side are what's called a delta pattern. Let's concentrate
on a single row to better understand how the game works.
 - The pattern `1` means there's only a single tile of the given color, but the
position is unknown.
 - The pattern `1 1` means there are two tiles in that row, but their positions and
the gap between them is unknown.
 - The pattern `2` means there are two tiles and they're next to each other without a 
gap between them.
 - In color mode the pattern `1 1`, where each `1` has a different color, means there
are two tiles of the specified colors in that row and they may or may not have a gap
between them.
5. Complete the delta pattern both vertically and horizontally to complete a level.
Tiles are moved by sliding rows or columns vertically and horizontally. 

# History
## The concept
History is an ironic term, as I first had the idea of 'NoNo! RotoGram!' when I was
studying for a history exam. I stopped immediately and went off to code the first
prototype of the core gameplay. After about two hours I had a prototype with a
randomized black and white board and sliding, just as it is today. The concept itself
came to me likely due too my interest in puzzles. A day or two earlier I solved a
couple real nonograms and I suddenly got the idea of combining a nonogram with a
"2D Rubik's cube", or a Rubik's square.

## Development
I used Löve2D as the engine for this project. Löve2D is an awesome engine written in 
C++, but it uses Lua for scripting. During the development I have only used a single
external library - one for parsing .json files (dkjson.lua), which are used extensively
by the game. Everything else, from the gameplay to the GUI was written solely by me.

## Artistic style
I'll be honest. There is no real artistic style. Everything you see in the game is
complete programmer art. There are no images to ease the design, everything is done
with vectors. In the beginning there were only rough squares that represented both the
gameplay and the GUI. Weeks have passed and I still had no real graphics, so I decided
I'd spend and entire week refining the user interface and visual design to be as 
pleasant to look at as possible. After some iterations, and a lot of critique from my
girlfriend, I made the abstract, black & white/pastel artistic style of 
'NoNo! RotoGram!' inspired by 1980's sci-fi.

## Libre
'NoNo! RotoGram!' is a free game both in the gratis and libre meaning. It's licensed
under the zlib license, meaning the source can be viewed and modified by anyone as
he/she chooses, but any modifications must be marked as such. Also, the license notice
must not be removed or altered from any further distributions. The full license is 
available in-game in the credits menu.

# Beta download
The game is currently in beta and will be released on April 2nd.
A beta version is downloadable here:

[itch.io](https://pi-pi3.itch.io/nono-rotogram)

# Videos
[![NoNo! Rotogram! showcase #1 - Endless mode](http://img.youtube.com/vi/mWH7FD0YMdw/0.jpg)](http://www.youtube.com/watch?v=mWH7FD0YMdw "NoNo! Rotogram! showcase #1 - Endless mode")
[![NoNo! Rotogram! showcase #2 - Endless color](http://img.youtube.com/vi/2BRMEKEkIhY/0.jpg)](http://www.youtube.com/watch?v=2BRMEKEkIhY "NoNo! Rotogram! showcase #2 - Endless color")
[![NoNo! Rotogram! showcase #3 - Levels](http://img.youtube.com/vi/-spwNZKd2sM/0.jpg)](http://www.youtube.com/watch?v=-spwNZKd2sM "NoNo! Rotogram! showcase #3 - Levels")
[![NoNo! Rotogram! showcase #4 - Color levels](http://img.youtube.com/vi/1PB6byVG6_8/0.jpg)](http://www.youtube.com/watch?v=1PB6byVG6_8 "NoNo! Rotogram! showcase #4 - Color levels")
[![NoNo! Rotogram! showcase #5 - Creating custom levels](http://img.youtube.com/vi/iQ1-e4V0gi4/0.jpg)](http://www.youtube.com/watch?v=iQ1-e4V0gi4 "NoNo! Rotogram! showcase #5 - Creating custom levels")
[![NoNo! RotoGram! showcase #6 - Editting palettes](http://img.youtube.com/vi/xy4b9450N8k/0.jpg)](http://www.youtube.com/watch?v=xy4b9450N8k "NoNo! RotoGram! showcase #6 - Editting palettes")

# Images
[img-01]: press/img-01.png
[img-02]: press/img-02.png
[img-03]: press/img-03.png
[img-04]: press/img-04.png
[img-05]: press/img-05.png
[img-06]: press/img-06.png

![Main menu][img-01]
![Gameplay in normal mode][img-02]
![Gameplay in color mode][img-03]
![Editor with a custom palette][img-04]
![One of the levels in-game][img-05]
![Another level][img-06]

# Logo & icon
[icon]: game_icon_512x512.png

![NoNo! RotoGram!][icon]

# About me
## Personal life
I was born in Poland and lived there for ten years, but at 10 I moved to Berlin with my
mom. At day I'm a student and I'm about to finish highschool. I've always had interest
in maths and computers, hence I'm planning to study computer sciences. 

## Game development
I've started making games around August 2014. That is when I participated in Ludum Dare
for the first time. Since I've been participating in game jams on and off, but only
once have I skipped Ludum Dare. Now I've put on the mantle of Drifting in Yellow and 
made 'NoNo! RotoGram!', which is intended to be my debut as a real gamedev. 

# Additional links
## Creator of the original font (Akt) used in 'NoNo! RotoGram!'
https://twitter.com/somepx

https://somepx.itch.io

## dkjson.lua - Library used for json parsing
https://dkolf.de/src/dkjson-lua.fsl/home

# Contact
## E-mail
mailto://walter.szymon.98@gmail.com

## Twitter
https://twitter.com/pi_pi314

## Creator page on itch.io
https://pi-pi3.itch.io

