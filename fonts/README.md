# DISCLAIMER
The font used in this game, Akt.ttf, is not a free font, hence it's not available
in this repository. If you want to use it in your own project, buy it from
somepx. This font and many more are available in his pixel font collections on
[itch.io](https://somepx.itch.io "Somepx's itch.io page").
